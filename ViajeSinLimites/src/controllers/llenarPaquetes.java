package controllers;
import java.io.IOException;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Ciudad;
import model.Hotel;
import model.PMF;
import model.Paquete;
import model.Usuario;


@SuppressWarnings("serial")
public class llenarPaquetes extends HttpServlet{
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
final PersistenceManager pm = PMF.get().getPersistenceManager();
		
		String est = "A";

		Query q = pm.newQuery(Paquete.class);
		q.setFilter("estReg == estRegParam");
		q.declareParameters("String estRegParam");
		
		
		RequestDispatcher rd= null;
		
		try{
			List<Paquete> listaP = (List<Paquete>) q.execute(est);
			
			
			req.setAttribute("listaP", listaP);
			
			

			rd = req.getRequestDispatcher("/listaPaquetes.jsp");
			rd.forward(req, resp);
			
		}
		catch(Exception e){
			
		}finally{
			q.closeAll();
			pm.close();
		}
	}

}
