package controllers;
import java.io.IOException;


import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.jdo.PersistenceManager;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;














import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.gson.Gson;

import model.Ciudad;
import model.PMF;
import model.Cliente;
import model.TipoUsuario;
import model.Usuario;

@SuppressWarnings("serial")
public class crearAdmin extends HttpServlet{
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		Map<String, Object> map = new HashMap<String,Object>();
		boolean exito = false;
		
		String nombre = req.getParameter("nombre");
		
		String correo = req.getParameter("correo");
		String dni = req.getParameter("dni");
		
		
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		
		Key keytipo = KeyFactory.stringToKey(req.getParameter("tipoA"));
		TipoUsuario tip = pm.getObjectById(TipoUsuario.class, keytipo);
		
		final Usuario nuevo = new Usuario();
		
		nuevo.setCorreo(correo);
		nuevo.setDocIdentidad(dni);
		nuevo.setNombre(nombre);
		nuevo.setTipo(tip);
		
		
		try{
			pm.makePersistent(nuevo);
		
			exito = true;
			map.put("isValid", exito);
			
			
		}catch(Exception e){
			System.out.println(e);
		
			
		}finally{
			pm.close();
		}
		write(resp,map);
		System.out.println("eroror??");
	}
	public void write(HttpServletResponse resp,Map <String,Object> map) throws IOException{
		resp.setContentType("application/json");
		resp.setCharacterEncoding("UTF-8");
		resp.getWriter().write(new Gson().toJson(map));
	}


}
