package controllers;
import java.io.IOException;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Ciudad;
import model.Hotel;
import model.PMF;
import model.Usuario;


@SuppressWarnings("serial")
public class cargarCLiEliminados extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		
		String est = "I";
		
		
	
		Query q = pm.newQuery(Usuario.class);
		q.setFilter("estReg == estRegParam");
		q.declareParameters("String estRegParam");
		
		
		RequestDispatcher rd= null;
		
		try{
			List<Usuario> clientesLE = (List<Usuario>) q.execute(est);
			
			System.out.println("=="+clientesLE.size());
			req.setAttribute("clientesLE", clientesLE);
			
			
			rd = req.getRequestDispatcher("/listaClientesEliminados.jsp");
			rd.forward(req, resp);
			
		}
		catch(Exception e){
			
		}finally{
			q.closeAll();
			pm.close();
		}
	}

}
