package controllers;


import java.io.IOException;

import javax.jdo.Query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.jdo.PersistenceManager;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

import model.PMF;
import model.Cliente;
import model.PaqueteCabecera;
import model.PaqueteDetalle;
import model.Usuario;

@SuppressWarnings("serial")
public class ConsultarPaquete extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
		Cliente cliente = (Cliente)getServletContext().getAttribute("cliente");
		Key keycliente = KeyFactory.stringToKey(cliente.getId());
		System.out.println(cliente.getId());
		System.out.println(keycliente);
		System.out.println("asdasddxx");
		
		Map<String, Object> map = new HashMap<String,Object>();
		boolean status = false;
		
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		final Query q = pm.newQuery(PaqueteDetalle.class);
		
		String estReg = "A";
		
		q.setOrdering("id descending");
		
		q.setFilter("estReg == estRegParam");
		q.declareParameters("String estRegParam");
		
		Usuario client = pm.getObjectById(Usuario.class, keycliente);
		
		try{
			
			@SuppressWarnings("unchecked")
			List<PaqueteDetalle> detalles =  (List<PaqueteDetalle>) q.execute(estReg);
			
			List<PaqueteCabecera> mispaquetes = new ArrayList<PaqueteCabecera>();
			
			//req.setAttribute("personas", cabeceras);
			//RequestDispatcher rd = req.getRequestDispatcher("/WEB-INF/listpersona01.jsp");
			//rd.forward(req, resp);
			
		
			
		
			map.put("exito", status);
			System.out.println(mispaquetes.size());
			//System.out.println(mispaquetes.get(0).getCabecera().getCreador());
			getServletContext().setAttribute("mispaquetes",mispaquetes);
		
		}catch(Exception e){
			System.out.println(e);
		}finally{
			q.closeAll();
			pm.close();
		}
		write(resp,map);
		
	}
	public void write(HttpServletResponse resp,Map <String,Object> map) throws IOException{
		resp.setContentType("application/json");
		resp.setCharacterEncoding("UTF-8");
		resp.getWriter().write(new Gson().toJson(map));
	}

}
