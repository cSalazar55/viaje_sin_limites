package controllers;


import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.jdo.PersistenceManager;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import model.PMF;
import model.Cliente;
import model.Rubro;

@SuppressWarnings("serial")
public class CrearRubro extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		Map<String, Object> map = new HashMap<String,Object>();
		boolean exito = false;
		
		String rubro = req.getParameter("rubro");
		System.out.println("asdasdrubro");
		
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		final Rubro rubro1 = new Rubro(rubro);
		
		try{
			pm.makePersistent(rubro1);
		
			exito = true;
			map.put("rubro", rubro1.getNombre());
			map.put("isValid", exito);	
			/*String mensaje_exito = "Bienvenido"+nuevoCliente.getNombres();
			map.put("mensaje_exito", mensaje_exito);*/
		
			
		}catch(Exception e){
			System.out.println(e);
			//String mensaje_error = "Ocurrio un error";
		
			
		}finally{
			pm.close();
		}
		
		write(resp,map);
		System.out.println(rubro1.getNombre());
	}
	public void write(HttpServletResponse resp,Map <String,Object> map) throws IOException{
		resp.setContentType("application/json");
		resp.setCharacterEncoding("UTF-8");
		resp.getWriter().write(new Gson().toJson(map));
	}
	
}
