package controllers;


import java.io.IOException;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;





import com.google.gson.Gson;

import model.PMF;
import model.Cliente;

import model.PaqueteDetalle;
import model.Proveedor;
import model.Rubro;

@SuppressWarnings("serial")
public class LlenarProv extends HttpServlet{
	public void doGet(HttpServletRequest req, HttpServletResponse resp)throws IOException {
		
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		final Query q = pm.newQuery(Proveedor.class);
		
		String ciudad = req.getParameter("countryname");
		
		q.setOrdering("id descending");
		
		q.setFilter("ciudad == ciudadParam");
		q.declareParameters("String ciudadParam");
		
		
		
		Map<String, String> ind = new LinkedHashMap<String, String>();
		
		try{
			@SuppressWarnings("unchecked")
			List<Proveedor> lista =  (List<Proveedor>) q.execute(ciudad);
			
			for(Proveedor prov: lista){
				
				
				  ind.put(prov.getId(), prov.getNombre());
			}
			
		
			
		}catch(Exception e){
			System.out.println(e);
		}finally{
			q.closeAll();
			pm.close();
		}
		
	    
	    String json = null ;
	 
	    	json= new Gson().toJson(ind);   
	    
	  
	    
	    resp.setContentType("application/json");
	    resp.setCharacterEncoding("UTF-8");
	    resp.getWriter().write(json);
	    
	    System.out.println("saddddddddddddddddddddddddddd");
	}

}
