package controllers;


import java.io.IOException;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;








import com.google.gson.Gson;

import model.PMF;
import model.Cliente;

import model.PaqueteDetalle;
import model.Rubro;

@SuppressWarnings("serial")
public class LlenarRubro  extends HttpServlet{
	public void doGet(HttpServletRequest req, HttpServletResponse resp)throws IOException {
		
		/*final PersistenceManager pm = PMF.get().getPersistenceManager();
		final Query q = pm.newQuery(Rubro.class);
		
		String estReg = "A";
		
		q.setOrdering("id descending");
		
		q.setFilter("estReg == estRegParam");
		q.declareParameters("String estRegParam");
		
		
		
		
			Map<String,String> lrubro = new LinkedHashMap<>
		try{
			@SuppressWarnings("unchecked")
			List<Rubro> rubros =  (List<Rubro>) q.execute(estReg);
			
			
			//req.setAttribute("personas", cabeceras);
			//RequestDispatcher rd = req.getRequestDispatcher("/WEB-INF/listpersona01.jsp");
			//rd.forward(req, resp);
			
			for(Rubro r1: rubros){
				
				
				
			}
			
		
			
		}catch(Exception e){
			System.out.println(e);
		}finally{
			q.closeAll();
			pm.close();
		}*/
		
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		final Query q = pm.newQuery(Rubro.class);
		
		String estReg = "A";
		
		q.setOrdering("id descending");
		
		q.setFilter("estReg == estRegParam");
		q.declareParameters("String estRegParam");
		
		
		String country=req.getParameter("countryname");
		Map<String, String> ind = new LinkedHashMap<String, String>();
		
		
		try{
			@SuppressWarnings("unchecked")
			List<Rubro> rubros =  (List<Rubro>) q.execute(estReg);
			
			for(Rubro r1: rubros){
				
				
				  ind.put(r1.getId(), r1.getNombre());
			}
			
		
			
		}catch(Exception e){
			System.out.println(e);
		}finally{
			q.closeAll();
			pm.close();
		}
		
	    
	    String json = null ;
	 
	    	json= new Gson().toJson(ind);   
	    
	  
	    
	    resp.setContentType("application/json");
	    resp.setCharacterEncoding("UTF-8");
	    resp.getWriter().write(json);
		    
		    
	}
	

}
