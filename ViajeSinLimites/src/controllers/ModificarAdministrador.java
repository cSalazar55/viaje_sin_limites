package controllers;


import java.io.IOException;

import javax.jdo.Query;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.jdo.PersistenceManager;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

import model.PMF;
import model.Cliente;
import model.Usuario;

@SuppressWarnings("serial")
public class ModificarAdministrador extends HttpServlet  {
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
		Map<String, Object> map = new HashMap<String,Object>();
		boolean exito = false;
		
	
		
		String nombres = req.getParameter("nombres");
		String apellidoP = req.getParameter("apellidoP");
		String apellidoM = req.getParameter("apellidoM");
		
		String dni = req.getParameter("dni");
		String telefono = req.getParameter("telefono");

		String direccion = req.getParameter("direccion");
		String fechaNac = req.getParameter("fechaNac");
		
		String correo = req.getParameter("correo");
		//Key key = KeyFactory.stringToKey(req.getParameter("id"));

		final PersistenceManager pm = PMF.get().getPersistenceManager();
		//Cliente found = pm.getObjectById(Cliente.class, key);
		
		Query q = pm.newQuery(Usuario.class);
		q.setFilter("correo == correoParam");
		q.declareParameters("String correoParam");
		
		try{
			List<Usuario> administrador = (List<Usuario>) q.execute(correo);
			for(Usuario p: administrador){
				
				p.setNombre(nombres);
				p.setApellidoPaterno(apellidoP);
				p.setApellidoMaterno(apellidoM);			
				p.setTelefono(telefono);		
				p.setDocIdentidad(dni);				
				p.setDireccion(direccion);		
				p.setFechaNacimiento(fechaNac);
				
				
				getServletContext().setAttribute("currentUser",p);
				
				
				
			}	
			exito = true;
			
			map.put("username", correo);
			map.put("isValid", exito);
		
			
		}catch(Exception e){
	
		}finally{
			q.closeAll();
		}
		

		write(resp,map);
	}
	public void write(HttpServletResponse resp,Map <String,Object> map) throws IOException{
		resp.setContentType("application/json");
		resp.setCharacterEncoding("UTF-8");
		resp.getWriter().write(new Gson().toJson(map));
	}

}
