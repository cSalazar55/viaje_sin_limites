package controllers;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.jdo.PersistenceManager;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.jdo.Query;
import javax.jdo.Transaction;

import org.apache.tools.ant.types.CommandlineJava.SysProperties;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.gson.Gson;

import model.PMF;
import model.Cliente;
import model.Paquete;
import model.PaqueteDetalle;
import model.Recurso;
import model.Usuario;

@SuppressWarnings("serial")
public class eliminarDetalle extends HttpServlet{
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		Map<String, Object> map = new HashMap<String,Object>();
		boolean exito = false;
		
		System.out.println("eliminannnddo");
		String[] detalles = req.getParameterValues("Aeliminar");
		
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		
		final Query q = pm.newQuery(Paquete.class);
		final Query q2 = pm.newQuery(PaqueteDetalle.class);

		q.setOrdering("id descending");
		q.setFilter("id == idParam");
		q.declareParameters("String idParam");
		
		q2.setOrdering("id descending");
		q2.setFilter("id == idParam");
		q2.declareParameters("String idParam");
		
		
		String idPaq = "";
		for(int i=0; i<detalles.length;i++){
			
				List<PaqueteDetalle> lpaqDet = (List<PaqueteDetalle>) q2.execute(detalles[i]);
				
				
				for(PaqueteDetalle p: lpaqDet){
					
					p.setEstReg("I");
					idPaq = p.getCabecera().getId();
				}	
				
				
			
				
				List<Paquete> lpaq = (List<Paquete>) q.execute(idPaq);
				for(Paquete  paq: lpaq){
					
					paq.setMiembros(paq.getMiembros()-1);
					
				}
				
				
			
			
		}
		exito = true;
		map.put("isValid", exito);
		
		q.closeAll();
		q2.closeAll();
		
		write(resp,map);
		
	}
	public void write(HttpServletResponse resp,Map <String,Object> map) throws IOException{
		resp.setContentType("application/json");
		resp.setCharacterEncoding("UTF-8");
		resp.getWriter().write(new Gson().toJson(map));
	}
}
