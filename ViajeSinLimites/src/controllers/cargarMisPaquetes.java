package controllers;
import java.io.IOException;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Ciudad;
import model.Hotel;
import model.PMF;
import model.PaqueteDetalle;
import model.Usuario;


@SuppressWarnings("serial")
public class cargarMisPaquetes extends HttpServlet{
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		
		String est = "A";

		
		Usuario us = (Usuario)getServletContext().getAttribute("currentUser");
		
		Query q = pm.newQuery(PaqueteDetalle.class);
		q.setFilter("cliente == clienteParam");
		q.declareParameters("String clienteParam");
		
		
		RequestDispatcher rd= null;
		try{
			List<PaqueteDetalle> misDetalles= (List<PaqueteDetalle>) q.execute(us);
			
			System.out.println(misDetalles.size());
			req.setAttribute("misDetalles", misDetalles);
			
			
			rd = req.getRequestDispatcher("/listaMisDetalles.jsp");
			rd.forward(req, resp);
			
		}
		catch(Exception e){
			
		}finally{
			q.closeAll();
			pm.close();
		}
	}

}
