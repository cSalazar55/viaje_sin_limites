package controllers;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import model.Ciudad;
import model.PMF;
import model.Rubro;
import model.Usuario;


@SuppressWarnings("serial")
public class CrearCiudad extends HttpServlet {
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
		Map<String, Object> map = new HashMap<String,Object>();
		boolean exito = false;
		
		String estado= "";
		String ciudad = req.getParameter("ciudad").toUpperCase();
		
	
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		
		

		Query q = pm.newQuery(Ciudad.class);
		q.setFilter("nombre == nombreParam");
		q.declareParameters("String nombreParam");
		
		try{
			List<Ciudad> ciudadl = (List<Ciudad>) q.execute(ciudad);
			System.out.println(ciudadl.size());
		
			if(ciudadl.size()!= 0){
				estado = "La ciudad ya existe";
			}
			else{
				final Ciudad ciudadnuevo = new Ciudad(ciudad);
				pm.makePersistent(ciudadnuevo);
				estado = "Exito";
				exito = true;
			}
		}
		catch(Exception e){
			
		}finally{
			q.closeAll();
			pm.close();
		}
		
		map.put("estado", estado);
		map.put("isValid", exito);
		write(resp,map);
		
		
	}
	public void write(HttpServletResponse resp,Map <String,Object> map) throws IOException{
		resp.setContentType("application/json");
		resp.setCharacterEncoding("UTF-8");
		resp.getWriter().write(new Gson().toJson(map));
	}

}
