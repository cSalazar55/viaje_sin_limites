package controllers;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.jdo.PersistenceManager;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.jdo.Query;
import javax.jdo.Transaction;

import org.apache.tools.ant.types.CommandlineJava.SysProperties;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.gson.Gson;

import model.PMF;
import model.Cliente;
import model.Paquete;
import model.PaqueteDetalle;
import model.Recurso;
import model.Usuario;

@SuppressWarnings("serial")
public class recuperarClientes extends HttpServlet{
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
		Map<String, Object> map = new HashMap<String,Object>();
		boolean exito = false;
		
		String[] listaRecuperar = req.getParameterValues("listaRecuperar");
		
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		
		final Query q = pm.newQuery(Usuario.class);
		q.setOrdering("id descending");
		q.setFilter("id == idParam");
		q.declareParameters("String idParam");
		
		for(int i=0; i<listaRecuperar.length;i++){
			List<Usuario> lus = (List<Usuario>) q.execute(listaRecuperar[i]);
			for(Usuario u3: lus){
				u3.setEstReg("A");
			}
		}
		
		
		
		
		exito = true;
		map.put("isValid", exito);
		
		
		q.closeAll();
		
		write(resp,map);
		
		}
		public void write(HttpServletResponse resp,Map <String,Object> map) throws IOException{
			resp.setContentType("application/json");
			resp.setCharacterEncoding("UTF-8");
			resp.getWriter().write(new Gson().toJson(map));
		}

}
