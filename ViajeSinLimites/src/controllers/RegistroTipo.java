package controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import javax.servlet.http.HttpSession;

import model.PMF;
import model.Recurso;
import model.TipoUsuario;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.google.gson.Gson;

@SuppressWarnings("serial")
public class RegistroTipo extends HttpServlet{
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
		
		Map<String, Object> map = new HashMap<String,Object>();
		boolean exito = false;
		
		String tipo = req.getParameter("tipo");
		
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		
		
		final Query q3 = pm.newQuery(TipoUsuario.class);

		
		final TipoUsuario nuevo =  new TipoUsuario(tipo);
		
		
		q3.setOrdering("id descending");
		q3.setFilter("nombre == nombreParam");
		q3.declareParameters("String nombreParam");
		
		try{
			
			@SuppressWarnings("unchecked")
			List<TipoUsuario> tipos = (List<TipoUsuario>) q3.execute(tipo);
			System.out.println(tipos.size());
			if(tipos.size() == 0 ){
				
				final Query qf = pm.newQuery(TipoUsuario.class);
				
				
				qf.setOrdering("id descending");
				qf.setFilter("estReg == estRegParam");
				qf.declareParameters("String estRegParam");
				
				
				
				pm.makePersistent(nuevo);	
				
				@SuppressWarnings("unchecked")
				List<TipoUsuario> listaTipos = (List<TipoUsuario>) qf.execute("A");
				
				System.out.println(listaTipos.size());
				getServletContext().setAttribute("listaTipos",listaTipos);
				
				exito = true;
				map.put("isValid", exito);
			}
			else{
				
				map.put("isValid", exito);
			}
			

		}catch(Exception e){
			System.out.println(e);
		}finally{
			q3.closeAll();
			pm.close();
		
		}

		
	
		write(resp,map);
		
	}
	public void write(HttpServletResponse resp,Map <String,Object> map) throws IOException{
		resp.setContentType("application/json");
		resp.setCharacterEncoding("UTF-8");
		resp.getWriter().write(new Gson().toJson(map));
	}

}
