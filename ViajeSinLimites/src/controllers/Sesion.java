package controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;







import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

@SuppressWarnings("serial")
public class Sesion extends HttpServlet{
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		 UserService us = UserServiceFactory.getUserService();
		 User user = us.getCurrentUser();
		 if(user == null)
			 resp.sendRedirect(us.createLoginURL(req.getRequestURI()));
		 else
			 resp.sendRedirect("inicio");
	}

}