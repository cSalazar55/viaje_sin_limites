package controllers;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.gson.Gson;

import model.Ciudad;
import model.Hotel;
import model.PMF;
import model.Rubro;
import model.TipoServicio;
import model.Usuario;


@SuppressWarnings("serial")
public class registroHotel extends HttpServlet {
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
		Map<String, Object> map = new HashMap<String,Object>();
		boolean exito = false;
		
		
		String nombre = req.getParameter("nombre");
		String direccion = req.getParameter("direccion");
		String correo = req.getParameter("correo");
		System.out.println("==="+req.getParameter("ciudadP"));
		
	
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		
		Key keyciudad = KeyFactory.stringToKey(req.getParameter("ciudadP"));

		final Hotel nuevo = new Hotel(nombre,correo,direccion);
		
		Ciudad c = pm.getObjectById(Ciudad.class, keyciudad);

		nuevo.setCiudad(c);
		System.out.println("123");
		try{
			pm.makePersistent(nuevo);
			exito=true;
			map.put("isValid", exito);
			System.out.println("oloaaaaaa");
		}catch(Exception e){
		
		}finally{
			pm.close();
		}
		
		
		write(resp,map);
		
	}
	public void write(HttpServletResponse resp,Map <String,Object> map) throws IOException{
		resp.setContentType("application/json");
		resp.setCharacterEncoding("UTF-8");
		resp.getWriter().write(new Gson().toJson(map));
	}
}
