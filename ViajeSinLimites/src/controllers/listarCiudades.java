package controllers;

import java.io.IOException;

import javax.jdo.Query;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.jdo.PersistenceManager;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

import model.Ciudad;
import model.PMF;


@SuppressWarnings("serial")
public class listarCiudades extends HttpServlet  {
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
		
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		
		String est = "A";

		Query q = pm.newQuery(Ciudad.class);
		q.setFilter("estReg == estRegParam");
		q.declareParameters("String estRegParam");
		
		
		RequestDispatcher rd= null;
		
		try{
			List<Ciudad> ciudadl = (List<Ciudad>) q.execute(est);
			
			
			req.setAttribute("ciudadl", ciudadl);
			req.setAttribute("ola",est);
			
			rd = req.getRequestDispatcher("/listaCiudades.jsp");
			rd.forward(req, resp);
			
		}
		catch(Exception e){
			
		}finally{
			q.closeAll();
			pm.close();
		}
		
		
	}
	

}
