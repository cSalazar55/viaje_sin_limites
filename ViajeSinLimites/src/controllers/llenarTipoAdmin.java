package controllers;
import java.io.IOException;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;





import com.google.gson.Gson;

import model.Ciudad;
import model.PMF;
import model.Cliente;

import model.PaqueteDetalle;
import model.Proveedor;
import model.Rubro;
import model.TipoUsuario;
import model.Usuario;

@SuppressWarnings("serial")
public class llenarTipoAdmin extends HttpServlet{
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp)throws IOException {
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		final Query q = pm.newQuery(TipoUsuario.class);
		
		String estReg = "A";
		
		q.setOrdering("id descending");
		
		q.setFilter("estReg == estRegParam");
		q.declareParameters("String estRegParam");
		
		Map<String, String> ind = new LinkedHashMap<String, String>();
		

		try{
			@SuppressWarnings("unchecked")
			List<TipoUsuario> lista =  (List<TipoUsuario>) q.execute(estReg);
			
			for(TipoUsuario us: lista){
				
				if(! us.getNombre().equals("cliente") )
				  ind.put(us.getId(), us.getNombre());
			}
			
		
			
		}catch(Exception e){
			System.out.println(e);
		}finally{
			q.closeAll();
			pm.close();
		}
		String json = null ;
		 
    	json= new Gson().toJson(ind);   
    
  
    
    resp.setContentType("application/json");
    resp.setCharacterEncoding("UTF-8");
    resp.getWriter().write(json);
    
	}

}
