package controllers;


import java.io.IOException;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.jdo.PersistenceManager;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.jdo.Query;
import javax.jdo.Transaction;



















import org.apache.tools.ant.types.CommandlineJava.SysProperties;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.gson.Gson;

import model.PMF;
import model.Cliente;
import model.Paquete;
import model.PaqueteDetalle;
import model.Recurso;
import model.Usuario;

@SuppressWarnings("serial")
public class RegistroDetalle extends HttpServlet {
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
	
		
		
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		
		String cab = req.getParameter("id_cabecera");
		
		Key keycabecera = KeyFactory.stringToKey(cab);
		Key keycliente = KeyFactory.stringToKey(req.getParameter("id_cliente"));
		
		
		final Query q3 = pm.newQuery(Paquete.class);

		q3.setOrdering("id descending");
		q3.setFilter("id == idParam");
		q3.declareParameters("String idParam");
		
		try{
			List<Paquete> lpaq = (List<Paquete>) q3.execute(cab);
			for(Paquete p: lpaq){
				
				p.setMiembros(p.getMiembros()+1);
			}				
		}catch(Exception e){
	
		}finally{
			q3.closeAll();
		}
		
			
			Paquete cabecera = pm.getObjectById(Paquete.class, keycabecera);
			Usuario cliente = pm.getObjectById(Usuario.class, keycliente);

			
			
			
			PaqueteDetalle p = new PaqueteDetalle();
			p.setCabecera(cabecera);
			p.setCliente(cliente);
			
			
			//p.setColor(copy);
			try{
				pm.makePersistent(p);
				getServletContext().setAttribute("exitoPaq", "Exitos al reservar");
				
			}catch(Exception e){		
			
			}finally{
				pm.close();
			}
			resp.sendRedirect("inicio_cliente.jsp");
			System.out.println("assssssssssssssssssssssssssssssss");
		

		
	}





}
