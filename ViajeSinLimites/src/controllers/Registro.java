package controllers;


import java.io.IOException;


import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.jdo.PersistenceManager;
import javax.servlet.http.HttpSession;





import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.gson.Gson;

import model.PMF;
import model.Cliente;
import model.TipoUsuario;
import model.Usuario;

@SuppressWarnings("serial")
public class Registro extends HttpServlet {
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
		Map<String, Object> map = new HashMap<String,Object>();
		boolean exito = false;
		
		String nombres = req.getParameter("nombres");
		String apellidoP = req.getParameter("apellidoP");
		String apellidoM = req.getParameter("apellidoM");
		String dni = req.getParameter("dni");
		String direccion = req.getParameter("direccion");
		String fecha = req.getParameter("fechaNacimiento");
		String telefono = req.getParameter("telefono");
		String correo = req.getParameter("correo");
		String username = req.getParameter("username");
		String tipo = req.getParameter("tipoUsuario");
		
		
		
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		
		final Usuario nuevoCliente =  new Usuario(nombres,apellidoP,apellidoM,dni,direccion,fecha,telefono,correo,username);

		if (tipo.equals("superadministrador")){
			TipoUsuario tip = new TipoUsuario(tipo);
			
			nuevoCliente.setTipo(tip);
		}
		else{
			
			Key keyCliente = KeyFactory.stringToKey(tipo);
			TipoUsuario cliente = pm.getObjectById(TipoUsuario.class, keyCliente);
			nuevoCliente.setTipo(cliente);
		}
	
		

		
		try{
			pm.makePersistent(nuevoCliente);
		
			exito = true;
			map.put("nombre", nuevoCliente.getNombre());
			map.put("isValid", exito);
			
			HttpSession misesion = req.getSession(true);
		
		
			getServletContext().setAttribute("cliente",nuevoCliente);
			
		
			/*String mensaje_exito = "Bienvenido"+nuevoCliente.getNombres();
			map.put("mensaje_exito", mensaje_exito);*/
		
			
		}catch(Exception e){
			System.out.println(e);
			//String mensaje_error = "Ocurrio un error";
		
			
		}finally{
			pm.close();
		}
	
		write(resp,map);
	
	}
	public void write(HttpServletResponse resp,Map <String,Object> map) throws IOException{
		resp.setContentType("application/json");
		resp.setCharacterEncoding("UTF-8");
		resp.getWriter().write(new Gson().toJson(map));
	}

}
