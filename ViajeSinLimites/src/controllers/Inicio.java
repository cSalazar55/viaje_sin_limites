package controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Paquete;
import model.Usuario;
import model.Permiso;
import model.Recurso;
import model.TipoUsuario;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

import model.Cliente;
import model.PMF;


@SuppressWarnings("serial")
public class Inicio extends HttpServlet{
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
		boolean sesion = false;
		boolean esregistrado = false;
		boolean esadministrador = false;
		
		boolean listo = false;
		boolean primero = true;
		
		String tipouser="";
		String tipouserId ="";
		
		String clienteId= "";
		
		/*session*/
		
		 UserService us = UserServiceFactory.getUserService();
		 User user = us.getCurrentUser();
		 
		 /*  */
		 
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		final Query q = pm.newQuery(Paquete.class);
		final Query q0 = pm.newQuery(Usuario.class);
	
		final Query qf = pm.newQuery(TipoUsuario.class);
		
	
		
		q0.setOrdering("id descending");
		q0.setFilter("estReg == estRegParam");
		q0.declareParameters("String estRegParam");
		
		qf.setOrdering("id descending");
		qf.setFilter("estReg == estRegParam");
		qf.declareParameters("String estRegParam");
		
		
		
		String estReg = "A";
	
		q.setOrdering("id descending");
		q.setFilter("estReg == estRegParam");
		q.declareParameters("String estRegParam");
		
		
		try{
			
			@SuppressWarnings("unchecked")
			List<Paquete> cabeceras = (List<Paquete>) q.execute(estReg);
			
			
			@SuppressWarnings("unchecked")
			List<TipoUsuario> listaTipos = (List<TipoUsuario>) qf.execute(estReg);
			
			System.out.println(listaTipos.size());
			//req.setAttribute("personas", cabeceras);
			//RequestDispatcher rd = req.getRequestDispatcher("/WEB-INF/listpersona01.jsp");
			//rd.forward(req, resp);
			getServletContext().setAttribute("listaTipos",listaTipos);
			getServletContext().setAttribute("cabeceras",cabeceras);
			System.out.println(cabeceras.size()+"ola");
		
		}catch(Exception e){
			System.out.println(e);
		}finally{
			q.closeAll();
			qf.closeAll();
		}
		final Query q1 = pm.newQuery(TipoUsuario.class);
		q1.setOrdering("id descending");
		q1.setFilter("nombre == nombreParam");
		q1.declareParameters("String nombreParam");
		
		
		try{
			
			@SuppressWarnings("unchecked")
			List<Usuario> listaUsuarios = (List<Usuario>) q0.execute(estReg);
			
			@SuppressWarnings("unchecked")
			List<TipoUsuario> listaTipos = (List<TipoUsuario>) q1.execute("cliente");
			
			System.out.println(listaUsuarios.size()+"---fgfg");
			
			if(listaUsuarios.size()==0){
				
				getServletContext().setAttribute("primero",primero);	
			}
			else{
				System.out.println("aqui");
				primero = false;
				getServletContext().setAttribute("primero",primero);
			}
			
			if(listaTipos.size() == 0){
				getServletContext().setAttribute("listo",listo);
			}else{
				for (TipoUsuario tipUsuario:listaTipos )
					clienteId = tipUsuario.getId();
				listo = true;
				getServletContext().setAttribute("listo",listo);
			}
			
			
		}catch(Exception e){
			System.out.println(e);
		}finally{
			q0.closeAll();
			q1.closeAll();
		}
		
		 if(user == null){
			 
			 getServletContext().setAttribute("sesion",sesion);
		
			 resp.sendRedirect("inicio.jsp");
			 
			 
		 }else{
			 
			 /* queries*/
			
			final Query q2 = pm.newQuery(Usuario.class);
			final Query q4 = pm.newQuery(Permiso.class);
		
			
			
			final Query q3 = pm.newQuery(Recurso.class);

			
		
			
			
			q3.setOrdering("id descending");
			q3.setFilter("estReg == estRegParam");
			q3.declareParameters("String estRegParam");
			
			String correo = user.getEmail();
			
			q2.setOrdering("id descending");
			q2.setFilter("correo == correoParam");
			q2.declareParameters("String correoParam");
			
	
			
			
			q4.setOrdering("id descending");
			q4.setFilter("tipo == tipoParam");
			q4.declareParameters("TipoUsuario tipoParam");
			
			
			/*/queries*/ 
			
			sesion = true;
			 
			getServletContext().setAttribute("sesion",sesion);	
			
			
			
			
			
			/*if(user.getNickname().equals("jhzapana")){
				
				esregistrado = true;
				esadministrador = true;
				
				try{
					
					@SuppressWarnings("unchecked")
					List<Recurso> recursos = (List<Recurso>) q3.execute(estReg);
					
					
					@SuppressWarnings("unchecked")
					List<TipoUsuario> tiposUsuarios = (List<TipoUsuario>) q1.execute(estReg);
					
					getServletContext().setAttribute("esadministrador",esadministrador);

					getServletContext().setAttribute("recursos",recursos);
					getServletContext().setAttribute("tiposUsuarios",tiposUsuarios);
					getServletContext().setAttribute("esregistrado",esregistrado);	
					
					
				
				}catch(Exception e){
					System.out.println(e);
				}finally{
					q3.closeAll();
					q1.closeAll();
				}
				resp.sendRedirect("inicio.jsp");
				
			}*/
			
			
			if(!primero){
				
				System.out.println("oalsps");
				TipoUsuario tipUs = new TipoUsuario();
				Usuario currentUser = new Usuario();
				try{
					boolean aprob = false;
					//buscar al usuario si esta registrado en el sistema
					@SuppressWarnings("unchecked")
					List<Usuario> lusuario = (List<Usuario>) q2.execute(correo);
					if(lusuario.size() !=0){
						for(Usuario uo : lusuario){
							if (uo.getEstReg().equals("A")){
								aprob= true;
							}
						}
					}
					
					//si esta registrado  se procede a verificar datos (tipo, permisos)
					if(aprob){
						esregistrado = true;
						
						for( Usuario usu : lusuario){
							currentUser = usu;
							tipUs = usu.getTipo(); 
						}
						tipouser = tipUs.getNombre();
						tipouserId = tipUs.getId();
						
						@SuppressWarnings("unchecked")
						List<Permiso> permisoUsuario = (List<Permiso>) q4.execute(tipUs);
						
						List<Recurso> lisRecursos = new ArrayList<Recurso>();
						List<String> misrecursos = new ArrayList<String>();
						
						for(Permiso perm : permisoUsuario){
							lisRecursos = perm.getRecursos();
						}
						
						for( int i = 0 ; i<lisRecursos.size();i++){
							misrecursos.add(lisRecursos.get(i).getNombre());
						}
						
						getServletContext().setAttribute("misrecursos",misrecursos);
						getServletContext().setAttribute("esregistrado",esregistrado);
						getServletContext().setAttribute("tipouser",tipouser);
						getServletContext().setAttribute("tipouserId",tipouserId);
						getServletContext().setAttribute("currentUser",currentUser);
					
					}
					else{
						getServletContext().setAttribute("esregistrado",esregistrado);
					
					}
				
				}catch(Exception e){
					System.out.println(e);
				}finally{
					
					q2.closeAll();
					q4.closeAll();
					pm.close();
				}
				
				
				
					 
			/*if((getServletContext().getAttribute("cliente"))==null){
				Cliente nuevo = new Cliente();
				getServletContext().setAttribute("cliente",nuevo);
			}*/
				
	
			}
			getServletContext().setAttribute("esregistrado",esregistrado);
			getServletContext().setAttribute("clienteId",clienteId);
			if((getServletContext().getAttribute("exitoDetalle"))==null){
				Boolean exito = false;
				getServletContext().setAttribute("exitoDetalle",exito);
			}
			
			resp.sendRedirect("inicio.jsp");
		 }
		
	}
	

}
