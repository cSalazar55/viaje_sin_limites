package controllers;


import java.io.IOException;

import javax.jdo.Query;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.jdo.PersistenceManager;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

import model.Ciudad;
import model.Hotel;
import model.PMF;
import model.Cliente;
import model.Paquete;


@SuppressWarnings("serial")
public class CrearPaquete extends HttpServlet{
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
		
		Map<String, Object> map = new HashMap<String,Object>();
		boolean exito = false;
		
		String nombre = req.getParameter("nombre");
		String descripcion = req.getParameter("descripcion");
		String fecha = req.getParameter("fecha");
		String administrador = req.getParameter("admin");
		

		

		String destino = req.getParameter("destino");
		String dias = req.getParameter("dias");
		String noches = req.getParameter("noches");
	
		String costo = req.getParameter("costo");
		
	
		
		
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		
		Key keyhotel = KeyFactory.stringToKey(req.getParameter("Photel"));
		
		Hotel h1 = pm.getObjectById(Hotel.class, keyhotel);
		
		final Paquete nuevo=  new Paquete(nombre,descripcion,fecha,destino,costo,dias,noches,administrador);
		nuevo.setHotel(h1);
		
		try{
			pm.makePersistent(nuevo);
	
			exito = true;
			map.put("exito", exito);
			
		
			
		}catch(Exception e){	
		}finally{
			pm.close();
		}
	
		
		write(resp,map);
		

		
	}
	public void write(HttpServletResponse resp,Map <String,Object> map) throws IOException{
		resp.setContentType("application/json");
		resp.setCharacterEncoding("UTF-8");
		resp.getWriter().write(new Gson().toJson(map));
	}

}
