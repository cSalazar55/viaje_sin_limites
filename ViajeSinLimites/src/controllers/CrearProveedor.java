package controllers;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.jdo.PersistenceManager;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.jdo.Transaction;












import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.gson.Gson;

import model.Ciudad;
import model.PMF;
import model.Cliente;
import model.PaqueteCabecera;
import model.PaqueteDetalle;
import model.Proveedor;
import model.Rubro;
import model.TipoServicio;

@SuppressWarnings("serial")
public class CrearProveedor extends HttpServlet{
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
		
		Map<String, Object> map = new HashMap<String,Object>();
		boolean exito = false;
		
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		
		String nombre = req.getParameter("nombre");
		String ruc = req.getParameter("ruc");
		String correo = req.getParameter("correo");
		String telefono = req.getParameter("telefono");
		String direccion = req.getParameter("direccion");
		String ciudad = req.getParameter("ciudadP");
		
	
		
		String cantidadS = req.getParameter("cantidad");
		int cantidad = Integer.parseInt(cantidadS);
		
		
	
		
		
		for(int i= 0;i < cantidad;i++){
			
			String tipo =req.getParameter("tipo"+(i+1));
			
		
			double costo = Double.parseDouble(req.getParameter("costo"+(i+1)));
			
			final TipoServicio nuevo = new TipoServicio(tipo,"",costo);
			
			try{
				pm.makePersistent(nuevo);
			
				
			}catch(Exception e){
				System.out.println(e);
				//String mensaje_error = "Ocurrio un error";
			
				
			}finally{
				
			}
			 
		}
		
		
		Key keyrubro = KeyFactory.stringToKey(req.getParameter("rubro"));
		Transaction tx = pm.currentTransaction();
		tx.begin();
		
		
		Rubro rubro1 = pm.getObjectById(Rubro.class, keyrubro);
		
		Proveedor pnuevo = new Proveedor(nombre,ruc,correo,telefono,ciudad,direccion);
		
		pnuevo.setRubro(rubro1);
		
		
		
		try{
			pm.makePersistent(pnuevo);
			tx.commit();	
		
		
			exito = true;
			map.put("proveedor", pnuevo.getNombre());
			map.put("isValid", exito);
			
		}catch(Exception e){
			System.out.println(e);
		
		}finally{
			
		try {
			if (tx.isActive())
				tx.rollback();
        } finally {
        	pm.close();
       		}
		}
		write(resp,map);
		
	}
	public void write(HttpServletResponse resp,Map <String,Object> map) throws IOException{
		resp.setContentType("application/json");
		resp.setCharacterEncoding("UTF-8");
		resp.getWriter().write(new Gson().toJson(map));
	}
	
}
