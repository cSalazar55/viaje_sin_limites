package controllers;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.jdo.PersistenceManager;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.jdo.Query;
import javax.jdo.Transaction;

import org.apache.tools.ant.types.CommandlineJava.SysProperties;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.gson.Gson;

import model.PMF;
import model.Cliente;
import model.Paquete;
import model.PaqueteDetalle;
import model.Recurso;
import model.Usuario;

@SuppressWarnings("serial")
public class llenarAdminE  extends HttpServlet{
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		String est = "I";

		Query q = pm.newQuery(Usuario.class);
		q.setFilter("estReg == estRegParam");
		q.declareParameters("String estRegParam");
		
		
		RequestDispatcher rd= null;
		try{
			List<Usuario> listaAE = (List<Usuario>) q.execute(est);
			
			
			req.setAttribute("listaAE", listaAE);
			
			
			rd = req.getRequestDispatcher("/listaAdminE.jsp");
			rd.forward(req, resp);
			
		}
		catch(Exception e){
			
		}finally{
			q.closeAll();
			pm.close();
		}

	}
}
