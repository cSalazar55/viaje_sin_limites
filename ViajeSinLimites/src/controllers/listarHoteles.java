package controllers;
import java.io.IOException;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Ciudad;
import model.Hotel;
import model.PMF;


@SuppressWarnings("serial")
public class listarHoteles  extends HttpServlet{
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		
		String est = "A";

		Query q = pm.newQuery(Hotel.class);
		q.setFilter("estReg == estRegParam");
		q.declareParameters("String estRegParam");
		
		
		RequestDispatcher rd= null;
		
		try{
			List<Hotel> hotell = (List<Hotel>) q.execute(est);
			
			
			req.setAttribute("hotell", hotell);
			
			
			rd = req.getRequestDispatcher("/listaHoteles.jsp");
			rd.forward(req, resp);
			
		}
		catch(Exception e){
			
		}finally{
			q.closeAll();
			pm.close();
		}
		
	}

}
