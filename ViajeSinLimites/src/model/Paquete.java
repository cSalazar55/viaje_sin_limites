package model;

import java.util.ArrayList;
import java.util.List;

import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.datanucleus.annotations.Unowned;

import model.Cliente;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class Paquete {
	
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key id;
	
	@Persistent
	private String nombre;
	
	@Persistent
	private String descripcion;
	
	@Persistent
	private String fecha;
	

	@Persistent
	@Unowned
	private String administrador;
	
	@Persistent
	private String ciudadDestino;
	
	@Persistent
	private String costo;
	
	@Persistent
	@Unowned
	private Hotel hotel;
	
	@Persistent
	@Unowned
	private List<PaqueteDet> detalle = new ArrayList<PaqueteDet>();
	
	@Persistent
	private String dias;
	
	@Persistent
	private String noches;
	
	@Persistent
	private String estReg;
	
	@Persistent
	private int miembros;
	

	public Paquete() {
		super();
		this.estReg = "A";
		this.miembros = 0;
		// TODO Auto-generated constructor stub
	}


	public Paquete(String nombre, String descripcion, String fecha,String destino,
			String costo, String dias, String noches,String admin) {
		super();
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.fecha = fecha;
		this.costo = costo;
		this.dias = dias;
		this.noches = noches;
		this.ciudadDestino = destino;
		this.administrador = admin;
		this.estReg = "A";
		this.miembros = 0;
		
	}

	public String getId() {
		return KeyFactory.keyToString(id);
	}
	

	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public String getFecha() {
		return fecha;
	}


	public int getMiembros() {
		return miembros;
	}


	public void setMiembros(int miembros) {
		this.miembros = miembros;
	}


	public void setFecha(String fecha) {
		this.fecha = fecha;
	}


	public String getAdministrador() {
		return administrador;
	}


	public void setAdministrador(String administrador) {
		this.administrador = administrador;
	}
	public String getCiudadDestino() {
		return ciudadDestino;
	}


	public void setCiudadDestino(String ciudadDestino) {
		this.ciudadDestino = ciudadDestino;
	}






	public String getCosto() {
		return costo;
	}


	public void setCosto(String costo) {
		this.costo = costo;
	}


	public Hotel getHotel() {
		return hotel;
	}


	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}


	public String getEstReg() {
		return estReg;
	}


	public void setEstReg(String estReg) {
		this.estReg = estReg;
	}


	public List<PaqueteDet> getDetalle() {
		return detalle;
	}


	public void setDetalle(List<PaqueteDet> detalle) {
		this.detalle = detalle;
	}


	public String getDias() {
		return dias;
	}


	public void setDias(String dias) {
		this.dias = dias;
	}


	public String getNoches() {
		return noches;
	}


	public void setNoches(String noches) {
		this.noches = noches;
	}
	
	
	
	
	
	
	

}
