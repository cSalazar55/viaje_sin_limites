package model;

import java.util.ArrayList;
import java.util.List;

import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.datanucleus.annotations.Unowned;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class Permiso {
	
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key id;
	

	@Persistent
	@Unowned
	private TipoUsuario tipo;
	

	@Persistent
	@Unowned
	private List<Recurso> recursos = new ArrayList<Recurso>();
	
	@Persistent
	@Unowned
	private String estReg;

	public Permiso() {
		super();
		// TODO Auto-generated constructor stub
		this.estReg = "A";
	}
	
	public String getId() {
		return KeyFactory.keyToString(id);
	}
	
	
	public TipoUsuario getTipo() {
		return tipo;
	}

	public void setTipo(TipoUsuario tipo) {
		this.tipo = tipo;
	}

	public List<Recurso> getRecursos() {
		return recursos;
	}

	public void setRecursos(List<Recurso> recursos) {
		this.recursos = recursos;
	}

	public String getEstReg() {
		return estReg;
	}

	public void setEstReg(String estReg) {
		this.estReg = estReg;
	}
	
	
	
	
	

}
