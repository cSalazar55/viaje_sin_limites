package model;

import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.datanucleus.annotations.Unowned;
import model.Cliente;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class PaqueteDetalle {
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key id;
	
	@Persistent
	@Unowned
	private Paquete cabecera;
	
	@Persistent
	@Unowned
	private Usuario cliente;
	
	@Persistent
	private String estReg;
	
	public PaqueteDetalle( ){
		super();
		this.estReg = "A";
	}
	
	public String getIdDetalle() {
		return KeyFactory.keyToString(id);
	}

	public void setIdDetalle(String iddet) {
		Key keyDetalle = KeyFactory.stringToKey(iddet);
		this.id = KeyFactory.createKey(keyDetalle,
		PaqueteDetalle.class.getSimpleName(), java.util.UUID.randomUUID().toString());
	}
	
	public Paquete getCabecera() {
		return this.cabecera;
	}
	
	public String cualeslaCabecera() {
		return this.cabecera.getNombre();
	}

	public void setCabecera(Paquete cabecera) {
		this.cabecera = cabecera;
	}
	
	public Usuario getCliente() {
		return this.cliente;
	}
	
	public String cualeselCliente() {
		return  this.cliente.getUsername();
	}

	public void setCliente(Usuario cliente) {
		this.cliente = cliente;
	}

	public String getEstReg() {
		return estReg;
	}

	public void setEstReg(String estReg) {
		this.estReg = estReg;
	}

}
