package model;

import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class Recurso {
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key id;
	
	@Persistent
	private String nombre;
	
	@Persistent
	private String estReg;

	public Recurso() {
		super();
		// TODO Auto-generated constructor stub
		this.estReg = "A";
	}

	public Recurso(String nombre) {
		super();
		this.nombre = nombre;
		this.estReg = "A";
	}
	
	
	public String getId() {
		return KeyFactory.keyToString(id);
	}
	

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEstReg() {
		return estReg;
	}

	public void setEstReg(String estReg) {
		this.estReg = estReg;
	}
	
	
	
	
	
	
	 
}
