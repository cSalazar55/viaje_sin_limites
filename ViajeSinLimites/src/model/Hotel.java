package model;
import java.util.ArrayList;
import java.util.List;

import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.datanucleus.annotations.Unowned;



@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class Hotel {
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key id;

	@Persistent
	private String nombre;
	
	@Persistent
	private String correo;
	
	@Persistent
	private String direccion;
	
	@Persistent
	@Unowned
	private List<TipoServicio> tipoServicio = new ArrayList<TipoServicio>();
	
	@Persistent
	private String estReg;
	
	@Persistent
	@Unowned
	private Ciudad ciudad;

	public Hotel() {
		super();
		this.estReg = "A";
		// TODO Auto-generated constructor stub
	}

	public Hotel(String nombre,String correo,String direccion) {
		super();
		this.nombre = nombre;
		this.correo = correo;
		this.direccion = direccion;
		this.estReg = "A";
	}

	public String getId() {
		return KeyFactory.keyToString(id);
	}
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<TipoServicio> getTipoServicio() {
		return tipoServicio;
	}

	public void setTipoServicio(List<TipoServicio> tipoServicio) {
		this.tipoServicio = tipoServicio;
	}

	public String getEstReg() {
		return estReg;
	}

	public void setEstReg(String estReg) {
		this.estReg = estReg;
	}

	public Ciudad getCiudad() {
		return ciudad;
	}

	public void setCiudad(Ciudad ciudad) {
		this.ciudad = ciudad;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
	
	
	
	
	
	
}
