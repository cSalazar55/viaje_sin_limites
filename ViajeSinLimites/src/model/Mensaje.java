package model;

import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.datanucleus.annotations.Owned;
import com.google.appengine.datanucleus.annotations.Unowned;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class Mensaje {
	
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key id;
	
	@Persistent
	@Unowned
	private Cliente cliente;
	
	@Persistent
	private String descripcion;
	
	@Persistent
	private String estReg;

	public Mensaje() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Mensaje(String descripcion) {
		super();
		this.descripcion = descripcion;
		this.estReg = "A";
	}
	
	public String getId() {
		return KeyFactory.keyToString(id);
	}
	

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getEstReg() {
		return estReg;
	}

	public void setEstReg(String estReg) {
		this.estReg = estReg;
	}
	
	
	
	
	
	

}
