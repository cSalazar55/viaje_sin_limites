package model;

import java.util.ArrayList;
import java.util.List;

import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.datanucleus.annotations.Unowned;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class Proveedor {
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key id;
	
	@Persistent
	private String nombre;
	
	
	@Persistent
	@Unowned
	private List<TipoServicio> tipoServicio = new ArrayList<TipoServicio>();
	
	
	@Persistent
	@Unowned
	private List<Viaje> alcance = new ArrayList<Viaje>();
	
	@Persistent
	private String ruc;
	
	@Persistent
	private String correo;
	
	@Persistent
	private String telefono;
	
	@Persistent
	private String direccion;
	
	@Persistent
	private String ciudad;
	
	@Persistent
	@Unowned
	private Rubro rubro;
	
	

	
	
	@Persistent
	private String estReg;

	public Proveedor() {
		super();
		// TODO Auto-generated constructor stub
		this.estReg = "A";
	}

	public Proveedor(String nombre, String ruc, String correo, String telefono,String ciudad,
			String direccion) {
		super();
		this.nombre = nombre;
		this.ruc = ruc;
		this.correo = correo;
		this.telefono = telefono;
		this.direccion = direccion;
		this.ciudad = ciudad;
		this.estReg = "A";
	}
	public String getId() {
		return KeyFactory.keyToString(id);
	}
	
	


	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getRuc() {
		return ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}


	public Rubro getRubro() {
		return rubro;
	}

	public void setRubro(Rubro rubro) {
		this.rubro = rubro;
	}

	public List<TipoServicio> getTipoServicio() {
		return tipoServicio;
	}

	public void setTipoServicio(List<TipoServicio> tipoServicio) {
		this.tipoServicio = tipoServicio;
	}

	public String getEstReg() {
		return estReg;
	}

	public void setEstReg(String estReg) {
		this.estReg = estReg;
	}

	public List<Viaje> getAlcance() {
		return alcance;
	}

	public void setAlcance(List<Viaje> alcance) {
		this.alcance = alcance;
	}
	
	
	

}
