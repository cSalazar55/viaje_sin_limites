package model;

import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class TipoServicio {
	
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key id;
	
	@Persistent
	private String nombre;
	
	@Persistent
	private double costo;
	
	@Persistent
	private String descripcion;
	
	@Persistent
	private String estReg;
	
	public TipoServicio(){
		super();
	}
	public TipoServicio(String nombre, String descripcion, double costo){
		super();
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.costo = costo;
		this.estReg = "A";		
	}
	
	public String getId() {
		return KeyFactory.keyToString(id);
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public double getCosto() {
		return costo;
	}
	public void setCosto(double costo) {
		this.costo = costo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getEstReg() {
		return estReg;
	}
	public void setEstReg(String estReg) {
		this.estReg = estReg;
	}
	
	
	
	
	

}
