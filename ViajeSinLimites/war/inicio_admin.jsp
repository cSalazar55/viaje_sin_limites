<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "model.*" %>
<%@ page import="java.util.*" %>
<%@ page import = "javax.servlet.http.HttpSession" %>
<%@ page import="java.text.SimpleDateFormat"%>


<%
//if( (getServletContext().getAttribute("votantes")) != null ){
Boolean listo = (Boolean)getServletContext().getAttribute("listo");

List<TipoUsuario> listaTipos = (List<TipoUsuario>)getServletContext().getAttribute("listaTipos");

String tipouser = (String)getServletContext().getAttribute("tipouser");
String tipouserId = (String)getServletContext().getAttribute("tipouserId");
Usuario currentUser = (Usuario)getServletContext().getAttribute("currentUser");
	
//}
%>
<%
  	 	Date dNow = new Date();
   		SimpleDateFormat ft = 
  	 	new SimpleDateFormat ("dd/MM/yyyy");
  	 	String currentDate = ft.format(dNow);
%>
<!DOCTYPE>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Inicio Administrador</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

	<link rel="stylesheet" href="css/ed-grid.css">
	
	<link rel="stylesheet" href="fonts/style.css" >
	<link rel="stylesheet" href="css/estilo_admin.css">
	<link rel="stylesheet" href="js/jquery-ui-1.11.4.custom/jquery-ui-3.css">
	

	

	<SCRIPT>
    function crearCampos(cantidad){
        var div = document.getElementById("campos_dinamicos");
        while(div.firstChild)div.removeChild(div.firstChild); // remover elementos;
            for(var i = 1, cantidad = Number(cantidad); i <= cantidad; i++){
            var salto = document.createElement("P");
            var input = document.createElement("input");
            var text = document.createTextNode("Servicio " + i + ": ");
            input.setAttribute("name", "tipo" + i);
            input.setAttribute("size", "12");
            input.setAttribute("type","text");
            input.className = "input";
            salto.appendChild(text);
            salto.appendChild(input);
            div.appendChild(salto);
            }
        }
    function crearCostos(cantidad){
        var div = document.getElementById("campos_dinamicos2");
        while(div.firstChild)div.removeChild(div.firstChild); // remover elementos;
            for(var i = 1, cantidad = Number(cantidad); i <= cantidad; i++){
            var salto = document.createElement("P");
            var input = document.createElement("input");
            var text = document.createTextNode("Costo Servicio " + i + ": ");
            input.setAttribute("name", "costo" + i);
            input.setAttribute("size", "12");
            input.className = "input";
            input.setAttribute("type","text");
            salto.appendChild(text);
            salto.appendChild(input);
            div.appendChild(salto);
            }
        }
    
 
 
  
</SCRIPT>

  <style>
  .ui-tabs-vertical { width: 100%; }
  .ui-tabs-vertical .ui-tabs-nav { padding: .2em .1em .2em .2em; float: left; width: 12em; }
  .ui-tabs-vertical .ui-tabs-nav li { clear: left; width: 100%; border-bottom-width: 1px !important; border-right-width: 0 !important; margin: 0 -1px .2em 0; }
  .ui-tabs-vertical .ui-tabs-nav li a { display:block; }
  .ui-tabs-vertical .ui-tabs-nav li.ui-tabs-active { padding-bottom: 0; padding-right: .1em; border-right-width: 1px; }
  .ui-tabs-vertical .ui-tabs-panel { padding: 1em; float: right; width: 70em;}
  </style>
 

</head>
<body>
	<div class="grupo total">
			<div class="links_div">
				
					<header>
						
						
	 
						<nav>
							<ul>
								<li><a href="/inicio"><span class="iconM-home"></span> Inicio</a></li>
								<li><a href="#tabs"><span class="iconM-tools"></span> Sistema</a></li>
								<li><a href="#tabs02"><span class="iconM-users"></span> Clientes</a></li>
								<li><a href="#tabs03"><span class="iconM-man"></span> Proveedores</a></li>						
								<li><a href="#tabs04" id = "click_paquete"><span class="iconM-colours"></span> Paquetes</a></li>
								<li><a href="#"><span class="iconM-news"></span> Reportes</a></li>
								<li><a href="#"><span class="iconM-new-message"></span> Comunicados</a></li>
								<li><a href="#"><span class="iconM-slideshare"></span> Foro</a></li>
								
							
							</ul>
						</nav>
						
					</header>
				</div>
				
				<section class="contenido wrapper">
				</section>
				
				
				
				
			
	</div>
	<div class="grupo" >
		
		<div class = "caja movil-100">
		
			<div id="respuesta_admin" >				
						
			</div>
			
		</div>
		<div class="caja movil-100">
		
			<div id="padre_tabs">
			
				<div  id = "t1" class="tabs_content">
					<div id="tabs" >
						  <ul>
						    <li><a href="#tabs-1">Mis Datos Personales</a></li>
						    <li><a href="#tabs-2">Rubros</a></li>
						    
						    <li><a href="#tabs-3">Tipos Usuarios</a></li>
						    <li><a href="#tabs-4">Ciudades</a></li>
						    <li><a href="#tabs-5" id = "Hotels">Hoteles</a></li>
						    <li><a href="#tabs-6" id = "Administradores">Administradores</a></li>
						    <li><a href="#tabs-7"> Recuperar Admin</a></li>
						  </ul>
						  <div id="tabs-1">
						  		<form action="modificarAdministrador" method= "post" id="form_modificarAdmin">
										
										<div id = "div_form">
											<label for ="nombres">Nombres:</label>
											<input type="text" name = "nombres" value= "<%=currentUser.getNombre()%>"><br>
										</div>
										<div id = "div_form">
											<label for ="apellidos">Apellidos Paterno:</label>
											<input type="text" name = "apellidoP" value= "<%=currentUser.getApellidoPaterno()%>"><br>
										</div>
										<div id = "div_form">
											<label for ="apellidos">Apellido Materno:</label>
											<input type="text" name = "apellidoM" value= "<%=currentUser.getApellidoMaterno()%>"><br>
										</div>
										
										<div id = "div_form">
											<label for ="dni">Dni:</label>
											<input type="text" name = "dni" value= "<%=currentUser.getDocIdentidad()%>"><br>
										</div>
										<div id = "div_form">
											<label for ="telefono">Telefono:</label>
											<input type="text" name = "telefono" value= "<%=currentUser.getTelefono()%>"><br>
										</div>
										<div id = "div_form">
											<label for ="correo">Correo:</label>
											<input type="text" name = "correo" value= "<%=currentUser.getCorreo()%>" readonly="readonly"><br>
										</div>
										<div id = "div_form">
											<label for ="direccion">Direccion:</label>
											<input type="text" name = "direccion" value ="<%=currentUser.getDireccion()%>" ><br>
										</div>
										<div id = "div_form">
											<label for ="fechaNac">Fecha Nacimiento:</label>
											<input type="text" name = "fechaNac" value ="<%=currentUser.getFechaNacimiento()%>"><br><br>
										</div>
										<br><br>
										<center><input type="submit" value= "Modificar" id = "btn_submit_modificarAdmin" class = "button"></center>
								</form>
						  </div>
						  <div id="tabs-2">
						  			<form action="crearRubro"  method = "get" id="form_crear_rubro">
							 				
										 <div id="div_form">
										 	<label for = "rubro">Rubro:</label>
										 	<input type = "text" name = "rubro"><br>
										 </div>
							 		
							 			<input type = "submit" class = "button" value = "Añadir Rubro">
							 		</form>	
						  </div>
						  <div id="tabs-3">
					  		<form action="registroTipo" method = "post" id = "form_regTipo">
												
								<div id="div_form">
									<label for = "tipo"> Nuevo Tipo:</label>
									<input type = "text" name = "tipo"><br>
								</div>
													
												
												
									 			
								<input type = "submit" class = "button" value = "CREAR ">
							</form>
							<h4>Tipos actuales</h4>
							<ul>
								<%for(TipoUsuario tp:listaTipos){ %>
									<li><%=tp.getNombre() %></li>
								<%} %>
							</ul>
					 	 </div>
					 	 <div id="tabs-4">
						  			<form action="crearCiudad"  method = "post" id="form_crear_ciudad">
							 				
										 <div id="div_form">
										 	<label for = "ciudad">Ciudad:</label>
										 	<input type = "text" name = "ciudad"><br>
										 </div>
							 		
							 			<input type = "submit" class = "button" value = "Añadir Ciudad">
							 		</form>	
							 		<div id = "div_listaCiudades">
							 		</div>
						 </div>
						<div id="tabs-5">
							<div id = "hot">
								
									<form action="registroHotel" method = "post" id = "for_reg_hotel">
										 <div id="div_form">
											 	<label for = "nombre">Nombre:</label>
											 	<input type = "text" name = "nombre"><br>
										 </div>
										  <div id="div_form">
											 	<label for = "correo">Correo:</label>
											 	<input type = "text" name = "correo"><br>
										 </div>
										  <div id="div_form">
											 	<label for = "direccion">Direccion:</label>
											 	<input type = "text" name = "direccion"><br>
										 </div>
										  <div id="div_form">
										  		<label for = "ciudadP">Ciudad:</label>
										  		<select name= "ciudadP" id = "ciudadP">
										  			<option>Elija</option>
										  		</select>
										   </div>
										   
										   	
										   <br><br>
										  <center> <input type="submit" value="Registrar Hotel"></center>
									
									</form>
							<br><br>
 			
							</div>
							
								<div id = "div_listaHoteles">
							</div>
						 </div>
						 
						<div id = "tabs-6">
								<div>
						  			<form action="crearAdmin"  method = "post" id="form_crear_admin">
							 				
										 <div id="div_form">
										 	<label for = "correo">Correo:</label>
										 	<input type = "text" name = "correo"><br>
										 </div>
										 
										 <div id="div_form">
										 	<label for = "nombre">Nombre:</label>
										 	<input type = "text" name = "nombre"><br>
										 </div>
										 
										 <div id="div_form">
										 	<label for = "dni">Dni:</label>
										 	<input type = "text" name = "dni"><br>
										 </div>
										 
										  <div id="div_form">
										 	<label for = "tipoA">Tipo:</label>
										 	<select name= "tipoA" id = "tipoA">
										  			<option>Elija</option>
										  		</select>
										 </div>
							 		
							 			<input type = "submit" class = "button" value = "Añadir Admin">
							 		</form>
							 	</div>	
							 	<div id = "div_listaAdmin">
							 	</div>
						
						</div>
						<div id="tabs-7">
						
								<div id = "div_listaAdmin2">
							 	</div>
							
						
						</div>
					
					</div>
				</div>
			
				<div id = "t2" class="tabs_content">
					<div id="tabs02">
					
						<ul>
						    <li><a href="#tabs02-1">Búsqueda</a></li>
						    <li><a href="#tabs02-2">Lista Clientes</a></li>
						    <li><a href="#tabs02-3">Eliminados</a></li>
					
						</ul>
						
						<div id="tabs02-1"></div>
						<div id="tabs02-2">
							<div id="misclientes">	
							</div>
						</div>
						<div id="tabs02-3">
						
							<div id="miseliminados">
							
							</div>
						</div>
				
					
					</div>
				</div>
				
				<div id = "t3" class="tabs_content">
					<div id="tabs03" >
						<ul>
						    <li><a href="#tabs03-1">Lista Proveedores</a></li>
						    <li><a href="#tabs03-2">Agregar Nuevo</a></li>
						    <li><a href="#tabs03-3">Busqueda</a></li>
						    <li><a href="#tabs03-4">Eliminados</a></li>
						</ul>
						
						<div id="tabs03-1"></div>
						<div id="tabs03-2"></div>
						<div id="tabs03-3"></div>
						<div id="tabs03-4"></div>
					
					</div>
				</div>
				<div id = "t4" class="tabs_content">
					<div id="tabs04">
						<ul>
						    <li><a href="#tabs04-1">Buscar</a></li>
						    <li><a href="#tabs04-2" id = "paqueteN">Agregar Nuevo</a></li>
						    <li><a href="#tabs04-3">Lista de paquetes</a></li>
						    <li><a href="#tabs04-3">Eliminados</a></li>
						</ul>
						
						<div id="tabs04-1">
						
						</div>
						<div id="tabs04-2">
							<form action="crearPaquete" method = "post" id = "form_agregar_paquete">
								 <div id="div_form">
									<label for = "nombre">Nombre:</label>
									<input type = "text" name = "nombre"><br>
								 </div>
								 <div id="div_form">
									<label for = "descripcion">Descripcion:</label>
									<input type = "text" name = "descripcion"><br>
								 </div>
								 <div id="div_form">
									<label for = "fecha">Fecha:</label>
									<input type = "text" name = "fecha" value = "<%= currentDate%>" readonly="readonly"><br>
								 </div>
								  <div id="div_form">
									<label for = "admin">Administrador:</label>
									<input type = "text" name = "admin" value = "<%=currentUser.getCorreo()%>" readonly="readonly"><br>
								 </div>
								  <div id="div_form">
									<label for = "destino">Destino:</label>
									<select name = "ciudadD" id = "ciudadD">
										<option>Elija</option>
									</select>
									<br>
								 </div>
								 
								 <div id="div_form">
									<label for = "dias">Dias:</label>
									<input type = "text" name = "dias" ><br>
								 </div>
								 
								 <div id="div_form">
									<label for = "noches">Noches:</label>
									<input type = "text" name = "noches" ><br>
								 </div>
								 
								 <div id="div_form">
									<label for = "costo">Costo (dólares):</label>
									<input type = "text" name = "costo" ><br>
								 </div>
								 
								 <div id="div_form">
									<label for = "Hotel">Hotel:</label>
									<select name = "Photel" id = "Photel">
										<option>Elija</option>
									</select>
								 </div>
								 <input type="submit" value = "Registrar Paquete">
							
							</form>
						
						</div>
						
						<div id="tabs04-3">
							Mis Paquetes:
							<div id = "paquetes_div"></div>
						
						</div>
						<div id="tabs04-4"></div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
		

		
		
		<script src="js/jquery-1.12.4.min.js"></script>
		<script src="js/jquery-ui-1.11.4.custom/jquery-ui.js"></script>
		<script src="js/main2.js"></script>
	
		<script>
		 	
			
			 $(function() {
			    $( "#tabs" ).tabs().addClass( "ui-tabs-vertical ui-helper-clearfix" );
			    $( "#tabs li" ).removeClass( "ui-corner-top" ).addClass( "ui-corner-left" );
			    
			    $( "#tabs02" ).tabs().addClass( "ui-tabs-vertical ui-helper-clearfix" );
			    $( "#tabs02 li" ).removeClass( "ui-corner-top" ).addClass( "ui-corner-left" );
			    
			    $( "#tabs03" ).tabs().addClass( "ui-tabs-vertical ui-helper-clearfix" );
			    $( "#tabs03 li" ).removeClass( "ui-corner-top" ).addClass( "ui-corner-left" );
			    
			    $( "#tabs04" ).tabs().addClass( "ui-tabs-vertical ui-helper-clearfix" );
			    $( "#tabs04 li" ).removeClass( "ui-corner-top" ).addClass( "ui-corner-left" );
			    
			   
			  
			 });
			
		
			 
			 	
  
		 	 
		</script>
</body>
</html>