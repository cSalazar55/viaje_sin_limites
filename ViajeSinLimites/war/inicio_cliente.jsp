<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ page import = "model.*" %>
<%@ page import = "javax.servlet.http.HttpSession" %>



<%
//if( (getServletContext().getAttribute("votantes")) != null ){
	Usuario currentUser = (Usuario)getServletContext().getAttribute("currentUser");
	String tipouser = (String)getServletContext().getAttribute("tipouser");
	String tipouserId = (String)getServletContext().getAttribute("tipouserId");
	
	
	String exitoPaq = (String)getServletContext().getAttribute("exitoPaq");
//}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html lang="es">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Inicio Cliente</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

	<link rel="stylesheet" href="css/ed-grid.css">
	<link rel="stylesheet" href="fonts/style.css"  >
	<link rel="stylesheet" href="css/estilo_cliente.css">
	<link rel="stylesheet" href="js/jquery-ui-1.11.4.custom/jquery-ui.css">
	<script src="js/jquery-1.12.4.min.js"></script>
	
</head>
<body>
	<div class="grupo total">
				<div class="links_div2">
				
					<header>
						
						
	 
						<nav>
							<ul>
								<li id = "ini"><a href="/inicio"><span class="iconM-home"></span> Inicio</a></li>
								<li><a href="#t11"><span class="iconM-v-card"></span> Perfil</a></li>
								<li><a href="#t22"><span class="iconM-chat"></span>Mensajes</a></li>
								<li><a href="#t33"><span class="iconM-shopping-cart"></span> Mis compras</a></li>
								<li><a href="#t44"><span class="iconM-wallet"></span>Rerservas</a></li>						
								<li><a href="#"><span class="iconM-compass"></span> Actividad</a></li>
								<li><a href="#"><span class="iconM-slideshare"></span> Foro</a></li>
								
							
							</ul>
						</nav>
						
					</header>
				</div>
				
				<section class="contenido wrapper">
				</section>
		</div>
		
		
		
		
		<% if (tipouser != null && tipouser.equals("cliente") && currentUser != null){%>
		
		
		<div class="grupo">
		
			
			<div class="caja movil-20" id = "cliente_left">
			
				 <center>
				 	<img src="img/user.jpg" class="movil-50">
				 	<br>
				 	<h2><%= currentUser.getUsername()%></h2>
				 	<br>
				</center> 	
				 	<div id="accordion">
							<h3>Último ingreso </h3>
								<div>
									 15 Julio
								
								</div>
								
									
								
							<h3>Mis Datos</h3>
								<div>
									<ul>
										<li><h4>Name:</h4></li>
											<center><%=currentUser.getNombre() %></center>
										<li><h4>Apellidos:</h4></li>
											<center><%=currentUser.getApellidoPaterno() %> <%=currentUser.getApellidoMaterno() %></center>
										<li><h4>Doc. Identidad:</h4></li>
											<center><%=currentUser.getDocIdentidad() %></center>
										<li><h4>Direccion:</h4></li>
											<center><%=currentUser.getDireccion() %></center>
										<li><h4>Fecha Nacimiento:</h4></li>
											<center><%=currentUser.getFechaNacimiento() %></center>
										<li><h4>Teléfono:</h4></li>
											<center><%=currentUser.getTelefono() %></center>
										<li><h4>Correo:</h4></li>
											<center><%=currentUser.getCorreo() %></center>
									</ul>
									<br>
										<a href="#" id = "log">Registrarse...</a>
							<br><br>
									<p><a href="#" id="dialog-link3" class="ui-state-default ui-corner-all"><span class="iconM-save"></span> Modificar</a></p>
									<div id="dialog3" title="Modificar Datos Personales">
											
										<form action="modificarCliente" method= "post" id="form_modificarCliente">
											<div id = "div_form">
												
												<input type="hidden" name = "username"  value= "<%= currentUser.getUsername() %>"><br>
											</div>
											<div id = "div_form">
												<label for ="nombres">Nombres:</label>
												<input type="text" name = "nombres"  value= "<%= currentUser.getNombre() %>"><br>
											</div>
											<div id = "div_form">
												<label for ="apellidoP">Ap.Paterno:</label>
												<input type="text" name = "apellidoP"  value= "<%= currentUser.getApellidoPaterno() %>"><br>
											</div>
											<div id = "div_form">
												<label for ="apellidoM">Ap.Materno:</label>
												<input type="text" name = "apellidoM"  value= "<%= currentUser.getApellidoMaterno() %>"><br>
											</div>
											<div id = "div_form">
												<label for ="dni">Doc.Identidad:</label>
												<input type="text" name = "dni"  value= "<%= currentUser.getDocIdentidad() %>"><br>
											</div>
											<div id = "div_form">
												<label for ="direccion">Direccion:</label>
												<input type="text" name = "direccion"  value= "<%= currentUser.getDireccion() %>"><br>
											</div>
											
											<div id = "div_form">
												<label for ="fecha">Fecha Nac:</label>
												<input type="text" name = "fecha"  value= "<%= currentUser.getFechaNacimiento() %>"><br>
											</div>
											<div id = "div_form">
												<label for ="telefono">Telefono:</label>
												<input type="text" name = "telefono"  value= "<%= currentUser.getTelefono() %>"><br>
											</div>
										
											
											<br><br>
											
											<center><input type="submit" value= "Modificar" id = "btn_submit_modificar" class = "button"></center>
										</form>
										<div id = "div_respModificar"></div>
									</div>
								</div>
								
							
						</div>
						
				 
				 
				 
				 
			</div>
			
			
			<div class="caja movil-60 " id = "cliente_right">
			
				<div id = "respuesta_cliente">
					<%if(exitoPaq != null){ %>
						<%=exitoPaq%>
						
					<%getServletContext().setAttribute("exitoPaq", null);} %>
				
				</div>
				
				<div id="padre_tabs2">
			
					<div  id = "t11" class="tabs_content2 active">
						1
						<div>1</div>
					</div>
					
					<div  id = "t22" class="tabs_content2">
						2	<div>2</div>
					</div>
					
					
					<div  id = "t33" class="tabs_content2">
						3
						<div>3</div>
					</div>
					
					
					<div  id = "t44" class="tabs_content2">
						
					</div>
				
				
				
				</div>
			
			
				
			</div>
			
			<div class="caja movil-20">
				<div id="datepicker"></div>
				
			</div>
			
			<!--  <div class="caja movil-100" id="repuestaCliente">
				
			</div>
			<div class="caja movil-30 cajaA">
			<center>
				<button class="button bt" id = "btn_modificar"><center>Modificar datos</center></button>
				<a href="" class="button bt">Calendario</a>
				<a href="" class="button bt" id = "mispaquetes">Mis Paquetes</a>
				<a href="" class="button bt" id = "a">Paquetes</a>
				
				
			</center>
			</div>
			<div class="caja movil-70 cajaB" id = "idcajaB">
				
				<div id="modificardiv" >
					<form action="modificarCliente" method= "post" id="form_modificar">
						<div id = "div_form">
							<label for ="id">Id:</label>
							<input type="text" name = "id"  value= "<%= currentUser.getId() %>" readonly="readonly"><br>
						</div>
						<div id = "div_form">
							<label for ="nombres">Nombres:</label>
							<input type="text" name = "nombres"  value= "<%= currentUser.getNombre() %>"><br>
						</div>
						<div id = "div_form">
							<label for ="apellidoP">Apellido Paterno:</label>
							<input type="text" name = "apellidoP"  value= "<%= currentUser.getApellidoPaterno() %>"><br>
						</div>
						<div id = "div_form">
							<label for ="apellidoM">Apellido Materno:</label>
							<input type="text" name = "apellidoM"  value= "<%= currentUser.getApellidoMaterno() %>"><br>
						</div>
						<div id = "div_form">
							<label for ="telefono">Telefono:</label>
							<input type="text" name = "telefono"  value= "<%= currentUser.getTelefono() %>"><br>
						</div>
						<div id = "div_form">
							<label for ="correo">Correo:</label>
							<input type="text" name = "correo"  value= "<%= currentUser.getCorreo() %>"><br>
						</div>
						<div id = "div_form">
							<label for ="username">Username:</label>
							<input type="text" name = "username"  value= "<%= currentUser.getUsername() %>"  readonly="readonly "><br>
						</div>
						<div id = "div_form">
							<label for ="password">Password:</label>
							<input type="text" name = "password""  value= ""><br><br>
						</div>
						<br><br>
						
						<center><input type="submit" value= "Modificar" id = "btn_submit_modificar" class = "button"></center>
					</form>
				</div>
				<div id="div_mispaquetes">
				dff
						
				</div>
			</div>
			
		
		</div>-->
		<%}%>
		
		
		
		
		<script src="js/jquery-ui-1.11.4.custom/jquery-ui.js"></script>
		<script src="js/main.js"></script>
		<script>
		
			$( "#datepicker" ).datepicker({
				inline: true
			});
			
		 	 $( "#accordion" ).accordion({
		        heightStyle: "content",
		        collapsible: true
		     });
		 	$( "#dialog3" ).dialog({
		 		autoOpen: false,
		 		width: 500,
		 		buttons: [
		 			{
		 				text: "Ok",
		 				click: function() {
		 					$( this ).dialog( "close" );
		 				}
		 			},
		 			{
		 				text: "Cancel",
		 				click: function() {
		 					$( this ).dialog( "close" );
		 				}
		 			}
		 		]
		 	});
		 	$( "#dialog-link3" ).click(function( event ) {
		 		$( "#dialog3" ).dialog( "open" );
		 		
		 		event.preventDefault();
		 	});
		</script>
</body>
</html>