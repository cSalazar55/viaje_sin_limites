<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
         <%@ page import = "model.*" %>
<%@ page import="java.util.*" %>
    
<% 
	List<Paquete> listaP = (List<Paquete>)request.getAttribute("listaP");
	

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Paquetes</title>
	<link rel="stylesheet" href="jquery.dataTables.css" >
 	<script src="js/jquery-1.12.4.min.js"></script>
	<script type="text/javascript" charset="utf8" src="jquery.dataTables.js"></script>
</head>


<body>
	<h3>Mis Paquetes: <%= listaP.size() %></h3>
	 <table  id="example3">
				<thead> 
					<tr> 
						<th>Nombre</th> 
						<th>Dias</th> 
						<th>Noches</th> 
						<th>Hotel</th>
						<th>Costo</th>
						<th>Miembros</th>
					</tr> 
				</thead> 
				
				<tbody> 
					<%for(Paquete p: listaP) {%>
					
						<tr> 
							<td><%=p.getNombre() %></td> 
							<td><%=p.getDias() %></td> 
							<td><%=p.getNoches() %></td>
							<td><%=p.getHotel().getNombre()%></td> 
							<td><%=p.getCosto()%></td> 
							<td><%=p.getMiembros()%></td> 
						</tr> 
					
					
					<%} %>
					
				</tbody>
				
		</table>



	<script>
  $(document).ready( function () {
	    $('#example3').DataTable();
	} );
  </script>
	

</body>
</html>