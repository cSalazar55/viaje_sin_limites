$(document).ready(main);


function main () {
	

	
	$("#tabs04-1").load("llenarPaquetes");
	$("#div_listaCiudades").load("listarCiudades");
	$("#div_listaHoteles").load("listarHoteles");
	$("#div_listaAdmin").load("llenarAdmin");
	
	 
	$('.links_div').find('a').eq(1).add($('.tabs_content').eq(0)).addClass('active');
	$('.links_div').on('click','li',function(e){
		var i = $(this).index();

		if(i!=0){
		e.preventDefault();
		
		$(this).find('a').addClass('active');
		$(this).siblings().find('a').removeClass('active');
		
		
		$('.tabs_content').eq(i-1).addClass('active').siblings().removeClass('active');
		}
	});
	$('#form_regTipo').submit(function(){
		
		$.ajax({
			url: 'registroTipo',
			type: 'POST',
			dataType: 'json',
			data: $('#form_regTipo').serialize(),
			success: function(data){
				if(data.isValid){
					$('#respuesta_admin').html('Se agrego Correctamente');
					setTimeout ("window.location.href = 'inicio_admin.jsp';", 1000); 		
						
				}
				else{
					$('#respuesta_admin').html('<h2>El tipo ya existe</h2>');
				}
					
			
			}
			
		});
		return false;
		
	});
	
	
	
	$('#form_modificarAdmin').submit(function(){
		
		$.ajax({
			url: 'modificarAdministrador',
			type: 'POST',
			dataType: 'json',
			data: $('#form_modificarAdmin').serialize(),
			success: function(data){
				if(data.isValid){
					$('#respuesta_admin').html('Los Datos han  sido Modificados:'+data.username);
						
				}
				else{
					alert('Datos Incorrectos');
				}
					
			
			}
			
		});
		return false;
		
	});
	
	
	$('#form_crear_ciudad').submit(function(){
		
		$.ajax({
			url: 'crearCiudad',
			type: 'POST',
			dataType: 'json',
			data: $('#form_crear_ciudad').serialize(),
			success: function(data){
				if(data.isValid){
					$('#respuesta_admin').html("<h2>"+data.estado+"</h2>");
					$("#div_listaCiudades").load("listarCiudades");
						
				}
				else{
					$('#respuesta_admin').html("<h2>"+data.estado+"</h2>");
				}
					
			
			}
			
		});
		return false;
		
	});
	
	$('#for_reg_hotel').submit(function(){
		
		$.ajax({
			url: 'registroHotel',
			type: 'POST',
			dataType: 'json',
			data: $('#for_reg_hotel').serialize(),
			success: function(data){
				if(data.isValid){
					$('#respuesta_admin').html("<h2>Se agrego Correctamente</h2>");
					
					$("#div_listaHoteles").load("listarHoteles");
				}
				else{
					$('#respuesta_admin').html("<h2>Ocurrio unError</h2>");
				}
					
			
			}
			
		});
		return false;
		
	});

	$('#form_crear_admin').submit(function(){
		
		$.ajax({
			url: 'crearAdmin',
			type: 'POST',
			dataType: 'json',
			data: $('#form_crear_admin').serialize(),
			success: function(data){
				if(data.isValid){
					$('#respuesta_admin').html("<h2>Se agrego Correctamente</h2>");
					$("#div_listaAdmin").load("llenarAdmin");
				}
				else{
					$('#respuesta_admin').html("<h2>Ocurrio unError</h2>");
				}
					
			
			}
			
		});
		return false;
		
	});
	
	
	
	
$('#form_agregar_paquete').submit(function(){
		
		$.ajax({
			url: 'crearPaquete',
			type: 'POST',
			dataType: 'json',
			data: $('#form_agregar_paquete').serialize(),
			success: function(data){
				if(data.isValid){
					$('#respuesta_admin').html("<h2>Se agrego Correctamente</h2>");
					$("#tabs04-1").load("llenarPaquetes");
				}
				else{
					$('#respuesta_admin').html("<h2>Se agrego Correctamente</h2>");
					$("#tabs04-1").load("llenarPaquetes");
				}
					
			
			}
			
		});
		return false;
		
	});
	
	
	
	
$('#btn_gestionar_paquete').click(function(event) {
		
		$.ajax({
			url: 'gestionPaquete',
			type: 'GET',
			dataType: 'json',
			success: function(data){
				if(data.status){
				
					
					$('#respuesta_admin').html('Se encontraron: '+data.numero+' paquetes');
				}
				
			}
		});
		return false;
	});
	
	$('#mispaquetes').click(function(event) {
		
		$.ajax({
			url: 'consultarPaquete',
			type: 'GET',
			dataType: 'json',
			success: function(data){
				if(data.exito){
				
					$('#div_mispaquetes').load('misPaquetes.jsp');
					
				}
				
				
			}
		});
		return false;
	});
	
	
	$('#form_crearPaquete').submit(function(){
		
		$.ajax({
			url: 'crearPaquete',
			type: 'POST',
			dataType: 'json',
			data: $('#form_crearPaquete').serialize(),
			success: function(data){
				if(data.exito){
					$('#respuesta_admin').html('Paquete creado:' + data.paquete);
				}
				
			}
		});
		return false;
		
	});
	
	$('#form_crear_rubro').submit(function(){
		
		$.ajax({
			url: 'crearRubro',
			type: 'GET',
			dataType: 'json',
			data: $('#form_crear_rubro').serialize(),
			success: function(data){
				if(data.isValid){
					$('#respuesta_admin').html('<h3>Rubro creado:' + data.rubro+'</h3>');
				}
				
			}
		});
		return false;
		
	});
	
	$('#form_crear_proveedor').submit(function(){
		
		$.ajax({
			url: 'crearProveedor',
			type: 'POST',
			dataType: 'json',
			data: $('#form_crear_proveedor').serialize(),
			success: function(data){
				if(data.isValid){
					$('#respuesta_admin').html('<h3>Proveedor  creado exitosamente :' + data.proveedor+'</h3>');
				}
				
			}
		});
		return false;
		
	});
	
	
	  (function(){
		  var $country="ol";
			 $.get('llenarTipoAdmin',{countryname:$country},function(responseJson) {   
	      	   var $select = $('#tipoA');                           
	             $select.find('option').remove();                          
	             $.each(responseJson, function(key, value) {               
	                 $('<option>').val(key).text(value).appendTo($select);      
	                  });
	          });
			 
	    }());

	 

	 
	  $('#Hotels').click(function(event) {  
		  var $country="ol";
			 $.get('llenarCiudad',{countryname:$country},function(responseJson) {   
	      	   var $select = $('#ciudadP');                           
	             $select.find('option').remove();                          
	             $.each(responseJson, function(key, value) {               
	                 $('<option>').val(key).text(value).appendTo($select);      
	                  });
	          });
		  
	  });
	  
	  $('#click_paquete').click(function(event) {  
		  var $country="ol";
			 $.get('llenarCiudad',{countryname:$country},function(responseJson) {   
	      	   var $select = $('#ciudadD');                           
	             $select.find('option').remove();                          
	             $.each(responseJson, function(key, value) {               
	                 $('<option>').val(key).text(value).appendTo($select);      
	                  });
	          });
			 
			
			 $.get('llenarComboHotel',{countryname:$country},function(responseJson) {   
	      	   var $select = $('#Photel');                           
	             $select.find('option').remove();                          
	             $.each(responseJson, function(key, value) {               
	                 $('<option>').val(key).text(value).appendTo($select);      
	                  });
	          });
		  
	  });
	  
	 
	
	 $('#selectC').change(function(event) {  
	        var $country=$("select#selectC").val();
	           $.get('llenarRubro',{countryname:$country},function(responseJson) {   
	        	   var $select = $('#select_rubros');                           
	               $select.find('option').remove();                          
	               $.each(responseJson, function(key, value) {               
	                   $('<option>').val(key).text(value).appendTo($select);      
	                    });
	            });
	        });
	 
	 $('#selectModProv').change(function(event) {  
	        var $country=$("select#selectModProv").val();
	           $.get('llenarProv',{countryname:$country},function(responseJson) {   
	        	   var $select = $('#select_proveedores');                           
	               $select.find('option').remove();                          
	               $.each(responseJson, function(key, value) {               
	                   $('<option>').val(key).text(value).appendTo($select);      
	                    });
	            });
	        });
};