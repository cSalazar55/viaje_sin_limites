<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
         <%@ page import = "model.*" %>
<%@ page import="java.util.*" %>
    
<% 
	List<PaqueteDetalle> misDetalles = (List<PaqueteDetalle>)request.getAttribute("misDetalles");
	
	
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Detalles</title>
	<link rel="stylesheet" href="jquery.dataTables.css" >
 	<script src="js/jquery-1.12.4.min.js"></script>
	<script type="text/javascript" charset="utf8" src="jquery.dataTables.js"></script>

</head>
<body>
	<h2>Mis Reservas:</h2>
		<form action = "eliminarDetalle" id ="form_eliminar_det" method = "post">
		 <table  id="examplea">
				<thead> 
					<tr> 
						<th>Nombre</th> 
						<th>Descripcion</th> 
						<th>Costo</th> 
						<th>Hotel</th> 
						<th>Eliminar</th>
					</tr> 
				</thead> 
				
				<tbody> 
					
					<%for(PaqueteDetalle h: misDetalles) {%>
						<%if(h.getEstReg().equals("A")) {%>
						<tr> 
							<td><%=h.getCabecera().getNombre() %></td> 
							<td><%=h.getCabecera().getDescripcion()%></td> 
							<td><%=h.getCabecera().getCosto() %></td> 
							<td><%=h.getCabecera().getHotel().getNombre() %></td> 
							<td><input type= "checkbox" value = "<%=h.getIdDetalle()%>" name = "Aeliminar" ></td>
						</tr> 
						<%} %>
					<%}%>
					
				</tbody>
			
				
				
		</table>

		<br><br>
			<center><input type = "submit" value = "Eliminar Seleccionados"></center>
		</form>
		<div id = "repuesta"></div>
	<script src="js/main3.js"></script>
	<script>
  $(document).ready( function () {
	    $('#examplea').DataTable();
	} );
  </script>
</body>
</html>