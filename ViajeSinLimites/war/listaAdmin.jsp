<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
     <%@ page import = "model.*" %>
<%@ page import="java.util.*" %>
    
<% 
	List<Usuario> listaU = (List<Usuario>)request.getAttribute("listaU");
	

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Admin</title>
	<link rel="stylesheet" href="jquery.dataTables.css" >
 	<script src="js/jquery-1.12.4.min.js"></script>
	<script type="text/javascript" charset="utf8" src="jquery.dataTables.js"></script>
</head>
<body>
	<h3>Mis Administradores:</h3>
		<form action = "eliminarAdmin" id ="form_eliminar_Admi" method = "post">
	 <table  id="example2">
				<thead> 
					<tr> 
						<th>Nombre</th> 
						<th>Correo</th> 
						<th>Tipo</th> 
						<th>Estado</th>
						<th>Eliminar</th>
					</tr> 
				</thead> 
				
				<tbody> 
					<%for(Usuario u: listaU) {%>
						<%if(!u.getTipo().getNombre().equals("cliente")){ %>
						<tr> 
							<td><%=u.getNombre() %></td> 
							<td><%=u.getCorreo() %></td> 
							<td><%=u.getTipo().getNombre() %></td>
							<td><%=u.getEstReg() %></td>  
							<td><input type= "checkbox" value = "<%=u.getId()%>" name = "listaEliminarA" ></td>  
						</tr> 
						<%} %>
					
					<%} %>
					
				</tbody>
				
		</table>
		<br><br>
			<center><input type = "submit" value = "Eliminar Seleccionados"></center>
		</form>
		<div id = "repuesta3">
		
		</div>



	<script>
  $(document).ready( function () {
	    $('#example2').DataTable();
	} );
  </script>

</body>
</html>