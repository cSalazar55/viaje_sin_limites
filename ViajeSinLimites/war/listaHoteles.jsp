<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
     <%@ page import = "model.*" %>
<%@ page import="java.util.*" %>
    
<% 
	List<Hotel> hotell = (List<Hotel>)request.getAttribute("hotell");
	
	
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Hoteles</title>
	<link rel="stylesheet" href="jquery.dataTables.css" >
 	<script src="js/jquery-1.12.4.min.js"></script>
	<script type="text/javascript" charset="utf8" src="jquery.dataTables.js"></script>

</head>
<body>
 <h2>Mis Hoteles:<%= hotell.size()%></h2>
		 <table  id="example">
				<thead> 
					<tr> 
						<th>Nombre</th> 
						<th>Ciudad</th> 
						<th>Direccion</th> 
						<th>Correo</th> 
					</tr> 
				</thead> 
				
				<tbody> 
					<%for(Hotel h: hotell) {%>
					
						<tr> 
							<td><%=h.getNombre() %></td> 
							<td><%=h.getCiudad().getNombre() %></td> 
							<td><%=h.getDireccion() %></td> 
							<td><%=h.getCorreo() %></td> 
						</tr> 
					
					<%} %>
					
				</tbody>
				
		</table>



	<script>
  $(document).ready( function () {
	    $('#example').DataTable();
	} );
  </script>
</body>
</html>