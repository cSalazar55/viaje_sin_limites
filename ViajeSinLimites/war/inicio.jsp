
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
  <%@ page import = "java.util.*" %>  
<%@ page import = "model.*" %>
<%@ page import = "javax.servlet.http.HttpSession" %>
<%@ page import = "com.google.appengine.api.users.*" %>
<%@ page import="java.util.List"%>


<% HttpSession misesion= request.getSession(); %>
<%
List<Paquete> cabeceras = (List<Paquete>)getServletContext().getAttribute("cabeceras");

Usuario currentUser = (Usuario)getServletContext().getAttribute("currentUser");
Boolean exito = (Boolean)getServletContext().getAttribute("exitoDetalle");


Boolean listo = (Boolean)getServletContext().getAttribute("listo");
Boolean primero = (Boolean)getServletContext().getAttribute("primero");



String clienteId = (String)getServletContext().getAttribute("clienteId");

Boolean sesion = (Boolean)getServletContext().getAttribute("sesion");
Boolean esregistrado = (Boolean)getServletContext().getAttribute("esregistrado");

UserService us = UserServiceFactory.getUserService();
User user = us.getCurrentUser();

%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Inicio</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

	<link href="fonts/style.css" rel="stylesheet" >

	<link  href="css/estilo.css"  rel="stylesheet">
	<link  href="css/ed-grid.css" rel="stylesheet">
	<link rel="stylesheet" href="js/jquery-ui-1.11.4.custom/jquery-ui-3.css">
	

	
	
</head>

	<body >
		
		<div class="grupo total">
			<div class="cabecera">

				<header>
					<div class = "contenedor">
					<div  id = "header_titulo">
						<div class="logo">
							<h1><a href="#" id="titulo"><span class="iconM-globe" ></span> Viajes SinLimites</a></h1>
						</div>
					</div>
 
					<nav class  = "menu">
						<ul>
							<li ><a href="inicio"><span class="iconM-home"></span> Inicio</a></li>
							<li><a href="#cotizacion_group"><span class="iconM-text-document"></span> Cotizaciones</a></li>
							<li><a href="#"><span class="iconM-aircraft"></span> Viajes  a Medida</a></li>						
							<li><a href="#"><span class="iconM-suitcase"></span> Paquetes</a></li>
							<li><a href="https://bitbucket.org/cSalazar55/viaje_sin_limites">Repositorio</a></li>
							
						
						</ul>
					</nav>
					</div>
				</header>
				<br><br>
			</div>
			
		<br><br>
		
			
			<div class="grupo total" id = "baner">
				<div class="caja tablet-75">
					<p>.</p>
				</div>
				
				<div class ="caja tablet-25" id="baner_caja">
					<div id = "respuesta_login">
						
					</div>
					<% if (sesion!= null && sesion){%>
					
						<h2><%= user.getNickname()%></h2>
						<a href= "<%= us.createLogoutURL("/inicio")%>">Cerrar..</a>
						<br>
						<%if(esregistrado!= null && esregistrado) {%>
							
							
							<%if(currentUser!= null && !currentUser.getTipo().getNombre().equals("cliente")) {%>
								<a href= "inicio_admin.jsp">Ir a perfil</a>
							<%}else{ %>
							
								<a href= "inicio_cliente.jsp">Ir a perfil</a>
							<%} %>
					
							
							
						
						<%}else{%>
							
							
							<br><br>
							<p><a href="#" id="dialog-link" class="ui-state-default ui-corner-all"><span class="ui-icon ui-icon-newwin"></span>Registrarse</a></p>
							
							<div id="dialog" title="Registro de Usuario">
							
									<form action = "registro" method = "post" id ="form_registro" >
											<div id="div_1">
												<fieldset>
													<legend>Personal</legend>
													<br>
													<div id = "div_form">
														<label for="nombres">Nombres:</label>
														<input type ="text" name= "nombres" /><br>
													</div>
													
													<div id = "div_form">
														<label for="apellidoP">Ap.Paterno:</label>
														<input type ="text" name= "apellidoP" /><br>
													</div>
												
													<div id = "div_form">
														<label for="apellidoM">Ap.Materno:</label>
														<input type ="text" name= "apellidoM"/><br>
													</div>
													
													<div id = "div_form">
														<%if(!listo) {%>
														
															<input type ="text" name= "tipoUsuario" value = "superadministrador" readonly="readonly" /><br>
														<%}else{ %>
															<input type ="hidden" name= "tipoUsuario" value = "<%=clienteId%>"/>
														<%} %>
													</div>
													<br>
													<center><div id="progressbar1" class = "movil-70"></div></center><br>
													
												
												</fieldset>	
											</div>
						
											<div id = "div_2">
												<fieldset>
													<legend>Contacto</legend>
													<br>
													<div id = "div_form">
														<label for="dni">Doc.Identidad:</label>
														<input type ="text" name= "dni"/><br>
													</div>
													<div id = "div_form">
														<label for="direccion">Direccion:</label>
														<input type ="text" name= "direccion"/><br>
													</div>
													<div id = "div_form">
														<label for="fechaNacimiento">Fecha Nac:</label>
														<input type ="text" name= "fechaNacimiento"  id = "datepicker" /><br>
													</div>
													<br>
														<center><div id="progressbar2" class = "movil-70"></div></center><br>
													
												</fieldset>	
									
											</div>
											
											<div id = "div_3">
												<fieldset>
													<legend>Contacto</legend>
													<br>
													<div id = "div_form">
														<label for="telefono">Telefono:</label>
														<input type ="text" name= "telefono"/><br>
													</div>
													
														<%if(user != null) {%>
													<div id = "div_form">
														<label for="correo">Correo:</label>
														<input type ="text" name= "correo"value =<%=user.getEmail() %> readonly="readonly"/><br>
													</div>
													<div id = "div_form">
														<label for="username">Username:</label>
														<input type ="text" name= "username"  value= <%=user.getNickname() %>  readonly="readonly"/><br>
													</div>
														<%} %>
														<br>
														<center><div id="progressbar3" class = "movil-70"></div></center><br>
														
													
																<center><input type="submit"  class= "button" value="Registrar" id = "btn_registro" /></center>
														
														<div id="respuesta">
														</div>
																
												</fieldset>	
									
											</div>
					
									</form>
					
					
					
									<center>
											<span class= "iconM-arrow-bold-left flechaIcono" id = "atras"></span>
											<span class= "iconM-arrow-bold-right flechaIcono" id = "siguiente"></span>
											
									</center>
							</div>
						<%}%>
						
					<%}else{%>
					<center>
						<span class="iconM-google-with-circle" id = "logo_google"></span><br><br>
						<a  href= "sesion" class= "btn_google"> Iniciar</a>
					
					</center>
					
					<%}%>
				</div>
			</div>
	
		</div>
		
		<!--  -->
		<br><br>
		
		
		<div class="grupo" id = "cotizacion_group">
		
			<div id="caja movil-100" class = "cotizacion_content">
				<center><h1><span class = "iconM-credit"></span>Cotizaciones</h1></center><br>
				<div id="tabs">
				  <ul>
				    <li><a href="#tabs-1">TRANSPORTE</a></li>
				    <li><a href="#tabs-2">ESTADIA</a></li>
				    <li><a href="#tabs-3">SERVICIOS</a></li>
				  </ul>
				  <div id="tabs-1">
				    <p>Proin elit arcu, rutrum commodo, vehicula tempus, commodo a, risus. Curabitur nec arcu. Donec sollicitudin mi sit amet mauris. Nam elementum quam ullamcorper ante. Etiam aliquet massa et lorem. Mauris dapibus lacus auctor risus. Aenean tempor ullamcorper leo. Vivamus sed magna quis ligula eleifend adipiscing. Duis orci. Aliquam sodales tortor vitae ipsum. Aliquam nulla. Duis aliquam molestie erat. Ut et mauris vel pede varius sollicitudin. Sed ut dolor nec orci tincidunt interdum. Phasellus ipsum. Nunc tristique tempus lectus.</p>
				  </div>
				  <div id="tabs-2">
				    <p>Morbi tincidunt, dui sit amet facilisis feugiat, odio metus gravida ante, ut pharetra massa metus id nunc. Duis scelerisque molestie turpis. Sed fringilla, massa eget luctus malesuada, metus eros molestie lectus, ut tempus eros massa ut dolor. Aenean aliquet fringilla sem. Suspendisse sed ligula in ligula suscipit aliquam. Praesent in eros vestibulum mi adipiscing adipiscing. Morbi facilisis. Curabitur ornare consequat nunc. Aenean vel metus. Ut posuere viverra nulla. Aliquam erat volutpat. Pellentesque convallis. Maecenas feugiat, tellus pellentesque pretium posuere, felis lorem euismod felis, eu ornare leo nisi vel felis. Mauris consectetur tortor et purus.</p>
				  </div>
				  <div id="tabs-3">
				    <p>Mauris eleifend est et turpis. Duis id erat. Suspendisse potenti. Aliquam vulputate, pede vel vehicula accumsan, mi neque rutrum erat, eu congue orci lorem eget lorem. Vestibulum non ante. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Fusce sodales. Quisque eu urna vel enim commodo pellentesque. Praesent eu risus hendrerit ligula tempus pretium. Curabitur lorem enim, pretium nec, feugiat nec, luctus a, lacus.</p>
				    <p>Duis cursus. Maecenas ligula eros, blandit nec, pharetra at, semper at, magna. Nullam ac lacus. Nulla facilisi. Praesent viverra justo vitae neque. Praesent blandit adipiscing velit. Suspendisse potenti. Donec mattis, pede vel pharetra blandit, magna ligula faucibus eros, id euismod lacus dolor eget odio. Nam scelerisque. Donec non libero sed nulla mattis commodo. Ut sagittis. Donec nisi lectus, feugiat porttitor, tempor ac, tempor vitae, pede. Aenean vehicula velit eu tellus interdum rutrum. Maecenas commodo. Pellentesque nec elit. Fusce in lacus. Vivamus a libero vitae lectus hendrerit hendrerit.</p>
				  </div>
				</div>
			
			</div>
		</div>
		
		<br>
		<div id="panel2">
		
		</div>
		<br><br>
		<div class="grupo paqueteA">
			<div class="caja movil-100" >
				<h2 class = "paquete_nombre"><span class=" iconM-images"></span> Paquetes Internacionales</h2><br>
			</div>
			<div class="caja movil-1-3 centrar-contenido" id ="ol1">
				
				<img src="img/pi1.jpg" alt="Imagen 1"   class = "img_rad">
				<br>
				<center>
				<h3 class = "titulo">India via Star Global</h3> 
				<h4 class = "descripcion"> 7Días – 6Noches</h4>
				<h4 class = "descripcion">Desde $ 2100.00 | S/.6600.5</h4><br>
				<button class = "btn_paq">Ver Info </button>
				</center>
			</div>
			<div class="caja movil-1-3">
							
				<img src="img/pi2.jpg" alt="Imagen 2"   class = "img_rad" >
				<br>
				<center>
				<h3 class = "titulo">Teotihuacán via Star México</h3> 
				<h4 class = "descripcion"> 5Días – 4Noches</h4>
				<h4 class = "descripcion">Desde $ 700.00 | S/.2400.0</h4><br>
				<button class = "btn_paq">Ver Info </button>
				</center>

			</div>
			<div class="caja movil-1-3">
							
				<img src="img/pi3.jpg" alt="Imagen 3"  class = "img_rad">
				<br>
				<center>
				<h3 class = "titulo">Sidney via Star Global</h3> 
				<h4 class = "descripcion"> Días – 2 Noches</h4>
				<h4 class = "descripcion">Desde $ 3500.00 | S/.11050.0</h4>
				<br>
				<button class = "btn_paq">Ver Info </button>
				</center>

			</div>
			
		</div>
	<br>
		<div class="grupo paqueteB">
			<div class="caja movi-100">
			<br><br>
				<strong><h2 class = "paquete_nombre"><span class=" iconM-images"> Paquetes Nacionales</h2><br></strong>
			</div>
			<div class="caja movil-1-3">
				
				<img src="img/pn1.jpg" alt=" Imagen 4"  >
				<br>
				<center>
				<h3 class = "titulo">Amazonas via Star Perú</h3> 
				<h4 class = "descripcion"> 4Días – 3 Noches</h4>
				<h4 class = "descripcion">Desde $ 230.00 | S/.750.0</h4>
				<br>
				<button class = "btn_paq">Ver Info </button>
				</center>

			</div>
			<div class="caja movil-1-3">
							
				<img src="img/pn2.jpg" alt="Imagen 5" >
				<br>
				<center>
				<h3 class = "titulo">Huancayo via Star Perú</h3> 
				<h4 class = "descripcion"> 3Días – 2 Noches</h4>
				<h4 class = "descripcion">Desde $ 160.00 | S/.500.5</h4>
				<br>
				<button class = "btn_paq">Ver Info </button>
				</center>

			</div>
			<div class="caja movil-1-3">
							
				<img src="img/pn3.jpg" alt="Imagen 6" >
				<br>
				<center>
				<h3 class = "titulo">Arequipa via Star Perú</h3> 
				<h4 class = "descripcion"> 3Días – 2 Noches</h4>
				<h4 class = "descripcion">Desde $ 197.00 | S/.689.5</h4>
			<br>
				<button class = "btn_paq">Ver Info </button>
				</center>

			</div>
			
		</div>
		<br>
		<div class="grupo paqueteC">
			<div class="caja movil-100" >
				<h2  class = "paquete_nombre"><span class=" iconM-images"> Paquetes Destacados</h2><br>
			</div>
		
		<% if (cabeceras != null && cabeceras.size()>0){%>
		<% for(int i=0; i<cabeceras.size();i++ ) {%>
			
			<div class="caja movil-1-3 centrar-contenido" id ="ol1">
				
				<img src="img/pi1.jpg" alt="Imagen 1"  border="3">
				<br>
				<center>
				<h3 class = "titulo"><%=cabeceras.get(i).getNombre() %></h3> 
				<h4 class = "descripcion"><%=cabeceras.get(i).getDescripcion() %></h4>
				<h4 class = "descripcion"><%=cabeceras.get(i).getDias()%>Días/<%=cabeceras.get(i).getNoches()%>Noches</h4>
				<h4 class = "descripcion">Desde<%=cabeceras.get(i).getCosto()%></h4>
				
				<% if(currentUser == null){%>
				
					<a href="#baner_caja" id= "btn_debe_registrarse" >Reservar</a>
			
				<%} else{%>
						<%if (currentUser.getTipo().getNombre().equals("cliente")){ %>
						<form action="registroDetalle" method= "post">
						
							<input type="hidden" name = "id_cabecera" value= "<%=cabeceras.get(i).getId()%>"><br>
							<input type="hidden" name = "id_cliente" value= "<%=currentUser.getId()%>"><br>
							<input type = "submit" id ="btn_submit_informar" value = "Reservar">
						</form>
						<%} %>
					
				<%} %>
				
				</center>
			</div>
		<%} %>
		<%} %>
		<div class="caja movil-100" id = "respuesta_reg">
			
		</div>
		</div>
		
		<br><br><br>
		<div class="grupo total">
				
			<footer>

				<div  class="caja tablet-total desde-tablet ">
					<img src="img/imgfooter.jpg" alt="Slider Imagen 1" class ="centro">
				</div>

				<div class="grupo tabla">
					<div class=" caja tablet-1-3 desde-tablet caja_f">
						<h3>Servicios</h3>
							<nav >
								<ul>
									<li><a href="#"><span class="icon-upload-to-cloud"></span>Paquetes Nacionales</a></li>
									<li><a href="#"><span class="icon-laptop"></span>Paquetes Internacionales</a></li>
									<li><a href="#"><span class="icon-tablet"></span>Reservaciones</a></li>
									<li><a href="#"><span class="icon-tv"></span>Viajes a Medida</a></li>
									<li><a href="#"><span class="icon-tv"></span>Movilidad</a></li>
								</ul>
							</nav>
					</div>

					<div class="caja tablet-1-3 desde-tablet caja_f">
						<h3>Nosotros</h3>
						<nav >
							<ul>
								<li><a href="#">Acerca de Viaje SinLimites</a></li>
								<li><a href="#">Visión</a></li>
						
								<li><a href="#">Consultas y Sugerencias</a></li>
							</ul>
						</nav>
					</div>

					<div class="caja movil-100 tablet-1-3 caja_f">
						<h3>Contacto</h3>
						<nav>
							<ul>
								<li><a href="#">Teléfono: (054)904523</a></li>
								<li><a href="#">E: informes@viajesinlimites.com</a></li>
						
								<li><a href="#">www.viajesinlimites.appspot.com</a></li>
							</ul>
						</nav>
					</div>
				</div>
				
				<div class="caja movil-100 cop">
					
					<center><h5>&copy; 2016 Viaje SinLimites</h5></center>
				</div>
			</footer>
		</div>
	<script src="js/jquery-1.12.4.min.js"></script>
	<script src="js/jquery-ui-1.11.4.custom/jquery-ui.js"></script>
	
	<script src="js/main.js"></script>
	<script>
	
	$( "#datepicker" ).datepicker({
		changeMonth: true,
		changeYear: true,
		inline: true
	});
	$( "#progressbar1" ).progressbar({
		value: 30
	});
	$( "#progressbar2" ).progressbar({
		value: 60
	});
	$( "#progressbar3" ).progressbar({
		value: 100
	});
	$( "#dialog" ).dialog({
 		autoOpen: false,
 		width: 500,
 		buttons: [
 			{
 				text: "Ok",
 				click: function() {
 					$( this ).dialog( "close" );
 				}
 			},
 			{
 				text: "Cancel",
 				click: function() {
 					$( this ).dialog( "close" );
 				}
 			}
 		]
 	});
 	$( "#dialog-link" ).click(function( event ) {
 		$( "#dialog" ).dialog( "open" );
 		event.preventDefault();
 	});
 	
 	 $( "#tabs" ).tabs({
 	      event: "mouseover"
 	    });
	</script>
	
	</body>

</html>
