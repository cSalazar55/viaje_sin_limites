<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
             <%@ page import = "model.*" %>
<%@ page import="java.util.*" %>
    
<% 
	List<Usuario> listaClientes = (List<Usuario>)request.getAttribute("clientesL");
	
	
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>LIsta</title>
<link rel="stylesheet" href="jquery.dataTables.css" >
 	<script src="js/jquery-1.12.4.min.js"></script>
	<script type="text/javascript" charset="utf8" src="jquery.dataTables.js"></script>
</head>
<body>
	<h2>Mis Clientes :</h2>
		<form action = "eliminarClientes" id ="form_eliminar_cli" method = "post">
		 <table  id="examplea">
				<thead> 
					<tr> 
						<th>Nombre</th> 
						<th>Apellidos</th> 
						<th>Dni</th> 
						<th>Correo</th>  
						<th>Eliminar</th>
					</tr> 
				</thead> 
				
				<tbody> 
					
					<%for(Usuario uu: listaClientes) {%>
						<%if( uu.getTipo().getNombre().equals("cliente")){ %>
						<tr> 
							<td><%=uu.getNombre() %></td> 
							<td><%=uu.getApellidoPaterno()%> <%=uu.getApellidoMaterno()%></td> 
							<td><%=uu.getDocIdentidad() %></td> 
							<td><%=uu.getCorreo() %></td> 
							<td><input type= "checkbox" value = "<%=uu.getId()%>" name = "listaEliminar" ></td>
						</tr> 
						<%} %>
					<%}%>
					
				</tbody>
			
				
				
		</table>

		<br><br>
			<center><input type = "submit" value = "Eliminar Seleccionados"></center>
		</form>
		<div id = "repuesta"></div>
	<script src="js/main3.js"></script>
	<script>
  $(document).ready( function () {
	    $('#examplea').DataTable();
	} );
  </script>

</body>
</html>